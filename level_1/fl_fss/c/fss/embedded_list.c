#include "embedded_list.h"
#include "../private-fss.h"
#include "macro.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_fl_fss_embedded_list_content_read_
  f_status_t fl_fss_embedded_list_content_read(const f_string_static_t buffer, f_state_t state, f_string_range_t * const range, f_fss_nest_t * const found, f_fss_delimits_t * const delimits, f_fss_comments_t * const comments) {
    #ifndef _di_level_1_parameter_checking_
      if (!range) return F_status_set_error(F_parameter);
      if (!found) return F_status_set_error(F_parameter);
      if (!delimits) return F_status_set_error(F_parameter);
      if (!comments) return F_status_set_error(F_parameter);
    #endif // _di_level_1_parameter_checking_

    f_status_t status = f_fss_skip_past_delimit(state, buffer, range);
    if (F_status_is_error(status) || status == F_none_eos || status == F_none_stop) return status;

    status = f_fss_nest_increase(state.step_small, found);
    if (F_status_is_error(status)) return status;

    f_array_lengths_t positions_start = f_array_lengths_t_initialize;

    status = f_array_lengths_increase(state.step_small, &positions_start);
    if (F_status_is_error(status)) return status;

    f_fss_objects_t objects = f_fss_objects_t_initialize;

    status = f_string_ranges_increase(state.step_small, &objects);

    if (F_status_is_error(status)) {
      f_array_lengths_resize(0, &positions_start);

      return status;
    }

    f_array_lengths_t slashes = f_array_lengths_t_initialize;

    status = f_array_lengths_increase(state.step_small, &slashes);

    if (F_status_is_error(status)) {
      f_array_lengths_resize(0, &positions_start);
      f_string_ranges_resize(0, &objects);

      return status;
    }

    const f_array_length_t delimits_used = delimits->used;
    const f_array_length_t comments_used = comments->used;

    f_array_length_t depth = 0;
    f_array_length_t position = 0;
    f_array_length_t position_previous = range->start;
    f_array_length_t line_start = range->start;
    f_array_length_t newline_last = range->start;
    f_array_length_t comment_delimit = 0;
    f_array_length_t slash_first = 0;
    f_array_length_t slash_last = 0;
    f_array_length_t before_list_open = position_previous;

    // 0x0 = is false for all, 0x1 = is first graph, 0x2 = is delimited comment, 0x4 = is open, 0x8 = is object, 0x10 = is delimited close.
    uint8_t is_state = 0x1;

    // Initialize depth 1 start position.
    // Positions_start.used is used as a max depth (such that positions_start.used == max depth + 1).
    positions_start.array[0] = range->start;
    positions_start.used = 1;

    slashes.array[0] = 0;
    slashes.used = 1;

    while (range->start <= range->stop && range->start < buffer.used) {

      if (state.interrupt) {
        status = state.interrupt((void *) &state, 0);

        if (F_status_set_fine(status) == F_interrupt) {
          status = F_status_set_error(F_interrupt);

          break;
        }
      }

      if (buffer.string[range->start] == f_fss_eol_s.string[0]) {
        if (is_state & 0x2) {
          status = f_array_lengths_increase(state.step_small, delimits);
          if (F_status_is_error(status)) break;

          delimits->array[delimits->used++] = comment_delimit;
        }

        newline_last = position_previous = range->start++;
        line_start = range->start;
        is_state = 0x1;
      }
      else if (buffer.string[range->start] == f_fss_delimit_slash_s.string[0] && (is_state & 0x1)) {
        slash_first = slash_last = position_previous = range->start++;
        slashes.array[depth] = 1;

        for (; range->start <= range->stop && range->start < buffer.used && (buffer.string[range->start] == f_fss_delimit_placeholder_s.string[0] || buffer.string[range->start] == f_fss_delimit_slash_s.string[0]); ++range->start) {

          if (state.interrupt) {
            status = state.interrupt((void *) &state, 0);

            if (F_status_set_fine(status) == F_interrupt) {
              status = F_status_set_error(F_interrupt);

              break;
            }
          }

          position_previous = range->start;

          if (buffer.string[position_previous] == f_fss_delimit_slash_s.string[0]) {
            slash_last = position_previous;
            ++slashes.array[depth];
          }
        } // for

        if (F_status_is_error(status) || range->start >= buffer.used || range->start > range->stop) break;

        // All slashes for an open are delimited (because it could represent a slash in the object name).
        // For example 'object {' = valid open, name 'object', 'object \{' represents 'object {', 'object \\{' = valid open, name 'object \', 'object \\\{' represents 'object \{', etc..
        // Only the first slash before a close is delimited, all others are maintained.
        // For example '}' = valid close, '\}' represents '}', '\\}' represents '\}', '\\\}' represents '\\}', '\\\\}' represents '\\\}', and so on..
        // When slash is odd and a (delimited) valid open/close is found, then save delimited positions and continue.
        if (buffer.string[range->start] == f_fss_eol_s.string[0]) {
          if (is_state & 0x2) {
            status = f_array_lengths_increase(state.step_small, delimits);
            if (F_status_is_error(status)) break;

            delimits->array[delimits->used++] = comment_delimit;
          }

          newline_last = position_previous = range->start++;
          line_start = range->start;
          is_state = 0x1;
        }
        else if (buffer.string[range->start] == f_fss_embedded_list_open_s.string[0] || buffer.string[range->start] == f_fss_embedded_list_close_s.string[0]) {
          before_list_open = position_previous;
          is_state &= ~0x15;

          if (buffer.string[range->start] == f_fss_embedded_list_open_s.string[0]) {
            is_state |= 0x4;
          }

          position_previous = range->start++;

          while (range->start <= range->stop && range->start < buffer.used) {

            if (state.interrupt) {
              status = state.interrupt((void *) &state, 0);

              if (F_status_set_fine(status) == F_interrupt) {
                status = F_status_set_error(F_interrupt);

                break;
              }
            }

            if (buffer.string[range->start] == f_fss_eol_s.string[0]) break;

            if (buffer.string[range->start] != f_fss_delimit_placeholder_s.string[0]) {
              status = f_fss_is_space(state, buffer, *range);
              if (F_status_is_error(status) || status == F_false) break;
            }

            position_previous = range->start;

            status = f_utf_buffer_increment(buffer, range, 1);
            if (F_status_is_error(status)) break;
          } // while

          if (F_status_is_error(status) || range->start >= buffer.used || range->start > range->stop) break;

          // This is a valid object open/close that has been delimited, save the slash delimit positions.
          if (buffer.string[range->start] == f_fss_eol_s.string[0]) {
            newline_last = range->start;
            line_start = newline_last + 1;

            if (is_state & 0x4) {
              if (slashes.array[depth] % 2 == 0) {
                is_state |= 0x8;
              }
              else {
                is_state &= ~0x8;
              }

              range->start = slash_first;

              status = f_array_lengths_increase_by((slashes.array[depth] / 2) + 1, delimits);
              if (F_status_is_error(status)) break;

              // Apply slash delimits, only slashes and placeholders should be present.
              while (slashes.array[depth]) {

                if (buffer.string[range->start] == f_fss_delimit_slash_s.string[0]) {
                  if (slashes.array[depth] % 2 == 1) {
                    delimits->array[delimits->used++] = range->start;
                  }

                  --slashes.array[depth];
                }

                // Delimit slashes and placeholders are required to be in the ASCII range.
                position_previous = range->start++;
              } // while

              // When slashes are even, the object is valid and needs to be processed.
              if (is_state & 0x8) {
                if (++depth >= objects.size) {
                  status = f_string_ranges_resize(depth + 2, &objects);
                }

                if (depth >= positions_start.size) {
                  if (F_status_is_error_not(status)) {
                    status = f_array_lengths_resize(depth + 2, &positions_start);
                  }
                }

                if (depth >= slashes.size) {
                  if (F_status_is_error_not(status)) {
                    status = f_array_lengths_resize(depth + 2, &slashes);
                  }
                }

                if (F_status_is_error(status)) break;

                objects.array[depth].start = line_start;
                objects.array[depth].stop = before_list_open;
                positions_start.array[depth] = newline_last + 1;
                slashes.array[depth] = 0;

                if (objects.used < depth) {
                  objects.used = depth;
                }

                if (positions_start.used < depth) {
                  positions_start.used = depth;
                }

                if (slashes.used < depth) {
                  slashes.used = depth;
                }
              }
            }
            else {
              is_state |= 0x10;
            }

            if (is_state & 0x12) {
              status = f_array_lengths_increase(state.step_small, delimits);
              if (F_status_is_error(status)) break;

              delimits->array[delimits->used++] = slash_last;
            }

            range->start = line_start;
            is_state = 0x1;
          }
        }
        else if (buffer.string[range->start] == f_fss_comment_s.string[0] && (is_state & 0x1)) {
          is_state |= 0x2;
          is_state &= ~0x1;
          comment_delimit = slash_first;
        }
        else {
          is_state &= ~0x1;
        }
      }
      else if (buffer.string[range->start] == f_fss_embedded_list_open_s.string[0]) {
        is_state &= ~0x1;
        before_list_open = position_previous;
        position_previous = range->start;

        status = f_utf_buffer_increment(buffer, range, 1);
        if (F_status_is_error(status)) break;

        while (range->start <= range->stop && range->start < buffer.used) {

          if (state.interrupt) {
            status = state.interrupt((void *) &state, 0);

            if (F_status_set_fine(status) == F_interrupt) {
              status = F_status_set_error(F_interrupt);

              break;
            }
          }

          if (buffer.string[range->start] == f_fss_eol_s.string[0]) break;

          if (buffer.string[range->start] != f_fss_delimit_placeholder_s.string[0]) {
            status = f_fss_is_space(state, buffer, *range);
            if (F_status_is_error(status) || status == F_false) break;
          }

          position_previous = range->start;

          status = f_utf_buffer_increment(buffer, range, 1);
          if (F_status_is_error(status)) break;
        } // while

        if (F_status_is_error(status) || range->start >= buffer.used || range->start > range->stop) break;

        if (buffer.string[range->start] == f_fss_eol_s.string[0]) {
          if (++depth >= objects.size) {
            status = f_string_ranges_resize(depth + 2, &objects);
            if (F_status_is_error(status)) break;
          }

          if (depth >= positions_start.size) {
            if (F_status_is_error_not(status)) {
              status = f_array_lengths_resize(depth + 2, &positions_start);
            }
          }

          if (depth >= slashes.size) {
            if (F_status_is_error_not(status)) {
              status = f_array_lengths_resize(depth + 2, &slashes);
            }
          }

          if (F_status_is_error(status)) break;

          objects.array[depth].start = line_start;
          objects.array[depth].stop = before_list_open;
          positions_start.array[depth] = range->start + 1;
          slashes.array[depth] = 0;

          if (objects.used <= depth) {
            objects.used = depth + 1;
          }

          if (positions_start.used <= depth) {
            positions_start.used = depth + 1;
          }

          if (slashes.used <= depth) {
            slashes.used = depth + 1;
          }

          if (is_state & 0x2) {
            status = f_array_lengths_increase(state.step_small, delimits);
            if (F_status_is_error(status)) break;

            delimits->array[delimits->used++] = comment_delimit;
          }

          newline_last = range->start;
          line_start = newline_last + 1;
          is_state = 0x1;
        }
        else {

          // No valid object close found, seek until EOL.
          status = f_fss_seek_to_eol(state, buffer, range);
          if (F_status_is_error(status)) break;

          if (is_state & 0x2) {
            status = f_array_lengths_increase(state.step_small, delimits);
            if (F_status_is_error(status)) break;

            delimits->array[delimits->used++] = comment_delimit;
          }

          newline_last = range->start;
          line_start = newline_last + 1;
          is_state = 0x1;
        }
      }
      else if (buffer.string[range->start] == f_fss_embedded_list_close_s.string[0] && (is_state & 0x1)) {
        while (range->start <= range->stop && range->start < buffer.used) {

          if (state.interrupt) {
            status = state.interrupt((void *) &state, 0);

            if (F_status_set_fine(status) == F_interrupt) {
              status = F_status_set_error(F_interrupt);

              break;
            }
          }

          position_previous = range->start;

          status = f_utf_buffer_increment(buffer, range, 1);
          if (F_status_is_error(status) || buffer.string[range->start] == f_fss_eol_s.string[0]) break;

          if (buffer.string[range->start] != f_fss_delimit_placeholder_s.string[0]) {
            status = f_fss_is_space(state, buffer, *range);
            if (F_status_is_error(status) || status == F_false) break;
          }
        } // while

        if (F_status_is_error(status) || range->start >= buffer.used || range->start > range->stop) break;

        if (buffer.string[range->start] == f_fss_eol_s.string[0]) {
          if (depth >= found->used) {
            status = f_fss_nest_resize(depth + 2, found);
          }

          if (found->depth[depth].used >= found->depth[depth].size) {
            if (F_status_is_error_not(status)) {
              status = f_fss_items_increase(state.step_small, &found->depth[depth]);
            }
          }

          position = found->depth[depth].used;

          if (found->depth[depth].array[position].content.size != 1) {
            if (F_status_is_error_not(status)) {
              status = f_string_ranges_resize(1, &found->depth[depth].array[position].content);
            }
          }

          if (F_status_is_error(status)) break;

          if (depth) {
            found->depth[depth].array[position].parent = found->depth[depth - 1].used;

            // Only assign object positions for nested objects.
            found->depth[depth].array[position].object.start = objects.array[depth].start;
            found->depth[depth].array[position].object.stop = objects.array[depth].stop;
          }

          // The Content is empty when line_start is the same as the start position.
          if (line_start == positions_start.array[depth]) {
            found->depth[depth].array[position].content.array[0].start = 1;
            found->depth[depth].array[position].content.array[0].stop = 0;
          }
          else {
            found->depth[depth].array[position].content.array[0].start = positions_start.array[depth];
            found->depth[depth].array[position].content.array[0].stop = newline_last;
          }

          found->depth[depth].array[position].content.used = 1;

          if (position >= found->depth[depth].used) {
            found->depth[depth].used++;
          }

          if (found->used < depth + 1) {
            found->used = depth + 1;
          }

          if (is_state & 0x2) {
            status = f_array_lengths_increase(state.step_small, delimits);
            if (F_status_is_error(status)) break;

            delimits->array[delimits->used++] = comment_delimit;
          }

          newline_last = range->start;
          line_start = newline_last + 1;
          is_state = 0x1;

          if (!depth) {
            status = f_utf_buffer_increment(buffer, range, 1);
            if (F_status_is_error(status)) break;

            private_macro_fl_fss_nest_return_on_overflow_delimited((buffer), (*range), (*found), positions_start, objects, slashes, F_none_eos, F_none_stop)

            f_array_lengths_resize(0, &positions_start);
            f_string_ranges_resize(0, &objects);
            f_array_lengths_resize(0, &slashes);

            return F_fss_found_content;
          }

          --depth;
        }
        else {

          // No valid object close found, seek until EOL.
          while (range->start <= range->stop && range->start < buffer.used) {

            if (state.interrupt) {
              status = state.interrupt((void *) &state, 0);

              if (F_status_set_fine(status) == F_interrupt) {
                status = F_status_set_error(F_interrupt);

                break;
              }
            }

            if (buffer.string[range->start] == f_fss_eol_s.string[0]) {
              if (is_state & 0x2) {
                status = f_array_lengths_increase(state.step_small, delimits);
                if (F_status_is_error(status)) break;

                delimits->array[delimits->used++] = comment_delimit;
              }

              newline_last = range->start;
              line_start = newline_last + 1;
              is_state = 0x1;

              break;
            }

            position_previous = range->start;

            status = f_utf_buffer_increment(buffer, range, 1);
            if (F_status_is_error(status)) break;
          } // while

          if (F_status_is_error(status) || range->start >= buffer.used || range->start > range->stop) break;
        }
      }
      else if (buffer.string[range->start] == f_fss_comment_s.string[0] && (is_state & 0x1)) {

        // The newline_last is initialized to the range->start, which may not actually be a new line.
        position = (buffer.string[newline_last] == f_string_eol_s.string[0]) ? newline_last + 1 : newline_last;

        status = f_fss_seek_to_eol(state, buffer, range);
        if (F_status_is_error(status)) break;

        if (range->start > range->stop || range->start >= buffer.used) {
          --range->start;
        }
        else {
          if (is_state & 0x2) {
            status = f_array_lengths_increase(state.step_small, delimits);
            if (F_status_is_error(status)) break;

            delimits->array[delimits->used++] = comment_delimit;
          }

          newline_last = range->start;
          line_start = newline_last + 1;
          is_state = 0x1;
        }

        status = f_string_ranges_increase(state.step_small, comments);
        if (F_status_is_error(status)) break;

        comments->array[comments->used].start = position;
        comments->array[comments->used++].stop = range->start++;
      }
      else if (buffer.string[range->start] != f_fss_eol_s.string[0]) {
        position_previous = range->start;

        if (is_state & 0x1) {
          status = f_fss_is_space(state, buffer, *range);

          if (status == F_false) {
            is_state &= ~0x1;
          }
        }

        if (F_status_is_error_not(status)) {
          status = f_utf_buffer_increment(buffer, range, 1);
        }

        if (F_status_is_error(status)) break;
      }
    } // while

    f_array_lengths_resize(0, &positions_start);
    f_string_ranges_resize(0, &objects);
    f_array_lengths_resize(0, &slashes);

    delimits->used = delimits_used;
    comments->used = comments_used;

    if (F_status_is_error(status)) return status;

    if (range->start > range->stop) {
      return depth ? F_status_set_error(F_end_not_nest_stop) : F_status_set_error(F_end_not_stop);
    }

    return depth ? F_status_set_error(F_end_not_nest_eos) : F_status_set_error(F_end_not_eos);
  }
#endif // _di_fl_fss_embedded_list_content_read_

#ifndef _di_fl_fss_embedded_list_content_write_
  f_status_t fl_fss_embedded_list_content_write(const f_string_static_t content, const uint8_t complete, const f_string_static_t * const prepend, const f_string_ranges_t * const ignore, f_state_t state, f_string_range_t * const range, f_string_dynamic_t * const destination) {
    #ifndef _di_level_1_parameter_checking_
      if (!range) return F_status_set_error(F_parameter);
      if (!destination) return F_status_set_error(F_parameter);
    #endif // _di_level_1_parameter_checking_

    f_status_t status = f_fss_skip_past_delimit(state, content, range);
    if (F_status_is_error(status)) return status;

    if (status == F_none_eos) {
      status = F_data_not_eos;
    }
    else if (status == F_none_stop) {
      status = F_data_not_stop;
    }

    if (range->start > range->stop || range->start >= content.used) {
      if (complete == f_fss_complete_full_e || complete == f_fss_complete_full_trim_e || complete == f_fss_complete_end_e) {
        const f_status_t status_allocation = f_string_dynamic_increase_by(state.step_small + 2, destination);
        if (F_status_is_error(status_allocation)) return status_allocation;

        destination->string[destination->used++] = f_fss_embedded_list_close_s.string[0];
        destination->string[destination->used++] = f_fss_embedded_list_close_end_s.string[0];
      }

      return status;
    }

    const f_array_length_t destination_used = destination->used;

    bool is_comment = F_false;
    bool ends_on_eol = F_false;
    bool has_graph = F_false;
    bool do_prepend = prepend ? F_true : F_false;

    f_array_length_t i = 0;
    f_array_length_t slash_count = 0;
    f_array_length_t start = 0;

    f_array_length_t r = 0;

    uint8_t width = 0;

    while (range->start <= range->stop && range->start < content.used) {

      if (state.interrupt) {
        status = state.interrupt((void *) &state, 0);

        if (F_status_set_fine(status) == F_interrupt) {
          status = F_status_set_error(F_interrupt);

          break;
        }
      }

      if (content.string[range->start] == f_fss_delimit_slash_s.string[0] && !is_comment) {
        slash_count = 1;

        if (do_prepend) {
          status = f_string_dynamic_append(*prepend, destination);
          if (F_status_is_error(status)) break;

          do_prepend = F_false;
        }

        status = f_string_dynamic_increase(state.step_large, destination);
        if (F_status_is_error(status)) break;

        destination->string[destination->used++] = content.string[range->start];

        for (++range->start; range->start <= range->stop && range->start < content.used; ++range->start) {

          if (state.interrupt) {
            status = state.interrupt((void *) &state, 0);

            if (F_status_set_fine(status) == F_interrupt) {
              destination->used = destination_used;

              return F_status_set_error(F_interrupt);
            }
          }

          if (content.string[range->start] == f_fss_delimit_placeholder_s.string[0]) continue;
          if (content.string[range->start] != f_fss_delimit_slash_s.string[0]) break;

          status = f_string_dynamic_increase(state.step_large, destination);

          if (F_status_is_error(status)) {
            destination->used = destination_used;

            return status;
          }

          destination->string[destination->used++] = f_fss_delimit_slash_s.string[0];
          ++slash_count;
        } // for

        if (content.string[range->start] == f_fss_embedded_list_open_s.string[0] || content.string[range->start] == f_fss_embedded_list_close_s.string[0]) {
          start = range->start++;

          status = f_fss_skip_past_space(state, content, range);
          if (F_status_is_error(status)) break;

          if (range->start >= content.used || range->start > range->stop || content.string[range->start] == f_fss_eol_s.string[0]) {

            // Increase by total slashes + 1 embedded list open/close.
            status = f_string_dynamic_increase_by(slash_count + 2, destination);
            if (F_status_is_error(status)) break;

            if (content.string[range->start] == f_fss_embedded_list_open_s.string[0]) {
              while (--slash_count) {
                destination->string[destination->used++] = f_fss_delimit_slash_s.string[0];
              } // while
            }

            destination->string[destination->used++] = f_fss_delimit_slash_s.string[0];
            destination->string[destination->used++] = content.string[start];

            range->start = start + 1;

            status = private_fl_fss_list_write_add_until_end(content, state, range, destination);
            if (F_status_is_error(status)) break;

            if (content.string[range->start] != f_fss_eol_s.string[0]) {
              has_graph = F_true;
            }

            continue;
          }

          // Increase by character at "start" and possible newline.
          status = f_string_dynamic_increase_by(state.step_small + 2, destination);
          if (F_status_is_error(status)) break;

          destination->string[destination->used++] = content.string[start];

          if (content.string[range->start] == f_fss_eol_s.string[0]) {
            destination->string[destination->used++] = f_fss_eol_s.string[0];
            ends_on_eol = F_true;
          }
          else {
            ends_on_eol = F_false;
          }

          range->start = start + 1;

          continue;
        }
      }
      else if ((content.string[range->start] == f_fss_embedded_list_open_s.string[0] || (!has_graph && content.string[range->start] == f_fss_embedded_list_close_s.string[0])) && !is_comment) {
        start = range->start++;

        if (do_prepend) {
          status = f_string_dynamic_append(*prepend, destination);
          if (F_status_is_error(status)) break;

          do_prepend = F_false;
        }

        has_graph = F_true;

        status = f_fss_skip_past_space(state, content, range);
        if (F_status_is_error(status)) break;

        if (range->start >= content.used || range->start > range->stop || content.string[range->start] == f_fss_eol_s.string[0]) {

          if (content.string[range->start] == f_fss_eol_s.string[0] && prepend) {
            do_prepend = F_true;
            ends_on_eol = F_true;
          }
          else {
            ends_on_eol = F_false;
          }

          if (ignore && ignore->used) {

            for (r = 0; r < ignore->used; ++r) {
              if (start >= ignore->array[r].start && start <= ignore->array[r].stop) break;
            } // for

            if (r < ignore->used) {
              status = f_string_dynamic_increase(state.step_large, destination);
              if (F_status_is_error(status)) break;

              destination->string[destination->used++] = content.string[start];
              range->start = start + 1;

              continue;
            }
          }

          // Increase by slash and extended list open and possible newline.
          status = f_string_dynamic_increase_by(state.step_small + 3, destination);
          if (F_status_is_error(status)) break;

          destination->string[destination->used++] = f_fss_delimit_slash_s.string[0];
          destination->string[destination->used++] = content.string[start];

          range->start = start + 1;

          status = private_fl_fss_list_write_add_until_end(content, state, range, destination);
          if (F_status_is_error(status)) break;

          continue;
        }

        status = f_string_dynamic_increase(state.step_large, destination);
        if (F_status_is_error(status)) break;

        destination->string[destination->used++] = content.string[start];
        range->start = start + 1;

        continue;
      }
      else if (content.string[range->start] == f_fss_comment_s.string[0] && !has_graph) {
        is_comment = F_true;
      }
      else if (content.string[range->start] == f_fss_eol_s.string[0]) {
        has_graph = F_false;
        is_comment = F_false;
      }
      else if ((status = f_fss_is_graph(state, content, *range)) == F_true) {
        has_graph = F_true;
      }
      else if (F_status_is_error(status)) {
        break;
      }

      if (content.string[range->start] != f_fss_delimit_placeholder_s.string[0]) {
        if (do_prepend) {
          status = f_string_dynamic_append(*prepend, destination);
          if (F_status_is_error(status)) break;

          do_prepend = F_false;
        }

        if (content.string[range->start] == f_fss_eol_s.string[0] && prepend) {
          do_prepend = F_true;
          ends_on_eol = F_true;
        }
        else {
          ends_on_eol = F_false;
        }

        width = macro_f_utf_byte_width(content.string[range->start]);

        status = f_string_dynamic_increase_by(width, destination);
        if (F_status_is_error(status)) break;

        for (i = 0; i < width; ++i) {
          destination->string[destination->used++] = content.string[range->start + i];
        } // for
      }

      status = f_utf_buffer_increment(content, range, 1);
      if (F_status_is_error(status)) break;
    } // while

    if (F_status_is_error(status)) {
      destination->used = destination_used;

      return status;
    }

    if (complete == f_fss_complete_full_e || complete == f_fss_complete_full_trim_e || complete == f_fss_complete_end_e) {
      status = f_string_dynamic_increase_by(state.step_small + 3, destination);
      if (F_status_is_error(status)) return status;

      if (!ends_on_eol) {
        destination->string[destination->used++] = f_fss_eol_s.string[0];
      }

      destination->string[destination->used++] = f_fss_embedded_list_close_s.string[0];
      destination->string[destination->used++] = f_fss_embedded_list_close_end_s.string[0];
    }

    if (range->start > range->stop) return F_none_stop;
    if (range->start >= content.used) return F_none_eos;

    return F_none;
  }
#endif // _di_fl_fss_embedded_list_content_write_

#ifndef _di_fl_fss_embedded_list_object_read_
  f_status_t fl_fss_embedded_list_object_read(const f_string_static_t buffer, f_state_t state, f_string_range_t * const range, f_fss_object_t * const found, f_fss_delimits_t * const delimits) {
    #ifndef _di_level_1_parameter_checking_
      if (!range) return F_status_set_error(F_parameter);
      if (!found) return F_status_set_error(F_parameter);
      if (!delimits) return F_status_set_error(F_parameter);
    #endif // _di_level_1_parameter_checking_

    return private_fl_fss_list_object_read(f_fss_embedded_list_open_s, buffer, state, range, found, delimits);
  }
#endif // _di_fl_fss_embedded_list_object_read_

#ifndef _di_fl_fss_embedded_list_object_write_
  f_status_t fl_fss_embedded_list_object_write(const f_string_static_t object, const uint8_t complete, f_state_t state, f_string_range_t * const range, f_string_dynamic_t * const destination) {
    #ifndef _di_level_1_parameter_checking_
      if (!range) return F_status_set_error(F_parameter);
      if (!destination) return F_status_set_error(F_parameter);
    #endif // _di_level_1_parameter_checking_

    f_status_t status = f_fss_skip_past_delimit(state, object, range);
    if (F_status_is_error(status)) return status;

    if (status == F_none_eos) {
      status = F_data_not_eos;
    }
    else if (status == F_none_stop) {
      status = F_data_not_stop;
    }

    if (status == F_data_not_stop || status == F_data_not_eos) {
      if (complete == f_fss_complete_partial_e || complete == f_fss_complete_partial_trim_e || complete == f_fss_complete_full_e || complete == f_fss_complete_full_trim_e) {
        {
          const f_status_t status_allocation = f_string_dynamic_increase_by(state.step_small + 2, destination);
          if (F_status_is_error(status_allocation)) return status_allocation;
        }

        destination->string[destination->used++] = f_fss_embedded_list_open_s.string[0];

        if (complete == f_fss_complete_full_e || complete == f_fss_complete_full_trim_e) {
          destination->string[destination->used++] = f_fss_embedded_list_open_end_s.string[0];
        }
      }

      return status;
    }

    // Ensure that there is room for a slash delimit, the object open character, and the end of line character.
    status = f_string_dynamic_increase_by(state.step_small + 4, destination);
    if (F_status_is_error(status)) return status;

    const f_array_length_t destination_used = destination->used;

    f_array_length_t slash_count = 0;

    bool ends_on_space = F_false;

    uint8_t width = 0;

    // Find the first graph character.
    while (range->start <= range->stop && range->start < object.used) {

      if (state.interrupt) {
        status = state.interrupt((void *) &state, 0);

        if (F_status_set_fine(status) == F_interrupt) {
          status = F_status_set_error(F_interrupt);

          break;
        }
      }

      if (object.string[range->start] == f_fss_comment_s.string[0]) {

        // When a comment is found, escape it.
        status = f_string_dynamic_increase(state.step_large, destination);
        if (F_status_is_error(status)) break;

        destination->string[destination->used++] = f_fss_delimit_slash_s.string[0];

        break;
      }

      status = f_fss_is_graph(state, object, *range);
      if (F_status_is_error(status)) break;

      if (status == F_true) break;

      // Objects will not have leading white spaces, but having this does not result in an invalid object, so just write the provided spaces.
      if (object.string[range->start] != f_fss_delimit_placeholder_s.string[0]) {
        if (object.string[range->start] == f_fss_eol_s.string[0]) {
          status = F_status_set_error(F_none_eol);

          break;
        }

        status = f_fss_is_space(state, object, *range);
        if (F_status_is_error(status)) break;

        if (status == F_true) {
          if (object.string[range->start] == f_fss_eol_s.string[0]) {
            status = F_status_set_error(F_none_eol);

            break;
          }

          width = macro_f_utf_byte_width(object.string[range->start]);

          status = f_string_dynamic_increase_by(width, destination);
          if (F_status_is_error(status)) break;

          memcpy(destination->string + destination->used, object.string + range->start, width);

          destination->used += width;
        }
      }

      status = f_utf_buffer_increment(object, range, 1);
      if (F_status_is_error(status)) break;
    } // while

    if (F_status_is_error(status)) {
      destination->used = destination_used;

      return status;
    }

    while (range->start <= range->stop && range->start < object.used) {

      if (state.interrupt) {
        status = state.interrupt((void *) &state, 0);

        if (F_status_set_fine(status) == F_interrupt) {
          status = F_status_set_error(F_interrupt);

          break;
        }
      }

      if (object.string[range->start] == f_fss_delimit_slash_s.string[0]) {
        slash_count = 1;

        for (++range->start; range->start <= range->stop && range->start < object.used; ++range->start) {

          if (state.interrupt) {
            status = state.interrupt((void *) &state, 0);

            if (F_status_set_fine(status) == F_interrupt) {
              destination->used = destination_used;

              return F_status_set_error(F_interrupt);
            }
          }

          if (object.string[range->start] == f_fss_delimit_placeholder_s.string[0]) continue;
          if (object.string[range->start] != f_fss_delimit_slash_s.string[0]) break;

          ++slash_count;
        } // for

        if (range->start > range->stop || range->start >= object.used) {

          // Slashes at the end of the object must be delimited to avoid delimiting the object close character.
          slash_count *= 2;
        }

        status = f_string_dynamic_increase_by(slash_count, destination);
        if (F_status_is_error(status)) break;

        while (--slash_count) {
          destination->string[destination->used++] = f_fss_delimit_slash_s.string[0];
        } // while

        if (range->start > range->stop || range->start >= object.used) {
          ends_on_space = F_false;

          break;
        }
      }

      if (object.string[range->start] != f_fss_delimit_placeholder_s.string[0]) {
        if (object.string[range->start] == f_fss_eol_s.string[0]) {
          status = F_status_set_error(F_none_eol);

          break;
        }

        status = f_fss_is_space(state, object, *range);
        if (F_status_is_error(status)) break;

        ends_on_space = (status == F_true);

        if (ends_on_space) {
          if (object.string[range->start] == f_fss_eol_s.string[0]) {
            status = F_status_set_error(F_none_eol);

            break;
          }
        }

        width = macro_f_utf_byte_width(object.string[range->start]);

        status = f_string_dynamic_increase_by(width, destination);
        if (F_status_is_error(status)) break;

        memcpy(destination->string + destination->used, object.string + range->start, width);

        destination->used += width;
      }

      status = f_utf_buffer_increment(object, range, 1);
      if (F_status_is_error(status)) break;
    } // while

    if (F_status_is_error(status)) {
      destination->used = destination_used;

      return status;
    }

    if (complete == f_fss_complete_partial_e || complete == f_fss_complete_partial_trim_e || complete == f_fss_complete_full_e || complete == f_fss_complete_full_trim_e) {
      if (complete == f_fss_complete_full_trim_e) {
        status = private_fl_fss_list_write_object_trim(destination_used, state, destination);

        if (F_status_is_error(status)) {
          destination->used = destination_used;

          return status;
        }

        // Prevent a space from being added post-trimming.
        ends_on_space = F_true;
      }

      status = f_string_dynamic_increase_by(state.step_small + 3, destination);

      if (F_status_is_error(status)) {
        destination->used = destination_used;

        return status;
      }

      if (!ends_on_space) {
        destination->string[destination->used++] = f_fss_space_s.string[0];
      }

      destination->string[destination->used++] = f_fss_embedded_list_open_s.string[0];

      if (complete == f_fss_complete_full_e || complete == f_fss_complete_full_trim_e) {
        destination->string[destination->used++] = f_fss_embedded_list_open_end_s.string[0];
      }
    }

    if (range->start > range->stop) return F_none_stop;
    if (range->start >= object.used) return F_none_eos;

    return F_none;
  }
#endif // _di_fl_fss_embedded_list_object_write_

#ifdef __cplusplus
} // extern "C"
#endif
