#include "fss_extended_list_read.h"
#include "private-common.h"
#include "private-print.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_fss_extended_list_read_print_at_
  void fss_extended_list_read_print_at(fll_program_data_t * const main, const f_array_length_t at, const f_fss_delimits_t delimits_object, const f_fss_delimits_t delimits_content, fss_extended_list_read_data_t * const data) {

    if (at >= data->contents.used) return;

    if ((data->option & fss_extended_list_read_data_option_object_d) || ((data->option & fss_extended_list_read_data_option_content_d) && (data->contents.array[at].used && data->contents.array[at].array[0].start <= data->contents.array[at].array[0].stop || (data->option & fss_extended_list_read_data_option_empty_d)))) {
      flockfile(main->output.to.stream);

      if (data->option & fss_extended_list_read_data_option_object_d) {
        fss_extended_list_read_print_at_object(main, data, at, delimits_object);
      }

      if (data->option & fss_extended_list_read_data_option_content_d) {
        if (data->contents.array[at].used && data->contents.array[at].array[0].start <= data->contents.array[at].array[0].stop) {
          if (data->contents.array[at].used && data->contents.array[at].array[0].start <= data->contents.array[at].array[0].stop) {
            fss_extended_list_read_print_content_ignore(main);

            f_print_except_in_dynamic_partial(data->buffer, data->contents.array[at].array[0], delimits_content, data->comments, main->output.to.stream);

            fss_extended_list_read_print_content_ignore(main);
          }
        }

        if (data->option & fss_extended_list_read_data_option_object_d) {
          const f_string_range_t before = fss_extended_list_read_print_at_determine_begin(main, data, at);

          if (before.start <= before.stop) {
            f_print_except_dynamic_partial(data->buffer, before, delimits_object, main->output.to.stream);
          }
        }
      }

      fss_extended_list_read_print_set_end(main, data);

      funlockfile(main->output.to.stream);
    }
  }
#endif // _di_fss_extended_list_read_print_at_

#ifndef _di_fss_extended_list_read_print_at_determine_begin_
  f_string_range_t fss_extended_list_read_print_at_determine_begin(fll_program_data_t * const main, fss_extended_list_read_data_t * const data, const f_array_length_t at) {

    f_string_range_t before = f_string_range_t_initialize;

    if (at < data->contents.used) {
      if (data->contents.used && data->contents.array[at].array[0].start <= data->contents.array[at].array[0].stop) {
        if (data->contents.array[at].array[0].stop + 1 < data->buffer.used) {
          before.start = data->contents.array[at].array[0].stop + 1;

          // Seek past any NULLs.
          while (before.start < data->buffer.used && !data->buffer.string[before.start]) ++before.start;
        }
        else {
          return before;
        }
      }
      else if (data->objects.array[at].start > data->objects.array[at].stop) {
        return before;
      }
      else {
        before.start = data->objects.array[at].stop;

        while (before.start < data->buffer.used && data->buffer.string[before.start] != f_fss_eol_s.string[0]) ++before.start;

        if (before.start >= data->buffer.used || data->buffer.string[before.start] != f_fss_eol_s.string[0]) {
          before.start = 1;
          before.stop = 0;

          return before;
        }

        ++before.start;
      }

      if (before.start < data->buffer.used) {
        before.stop = before.start;

        while (before.stop < data->buffer.used && data->buffer.string[before.stop] != f_fss_extended_list_close_s.string[0]) ++before.stop;

        if (before.stop < data->buffer.used && data->buffer.string[before.stop] == f_fss_extended_list_close_s.string[0]) {
          --before.stop;
        }
        else {
          before.start = 1;
          before.stop = 0;
        }
      }
      else {
        before.start = 1;
        before.stop = 0;
      }
    }

    return before;
  }
#endif // _di_fss_extended_list_read_print_at_determine_begin_

#ifndef _di_fss_extended_list_read_print_at_object_
  void fss_extended_list_read_print_at_object(fll_program_data_t * const main, fss_extended_list_read_data_t * const data, const f_array_length_t at, const f_fss_delimits_t delimits_object) {

    if (at >= data->objects.used) return;

    if ((data->option & fss_extended_list_read_data_option_content_d) && (!(data->option & fss_extended_list_read_data_option_original_d) || (data->option & fss_extended_list_read_data_option_trim_d))) {
      const f_string_range_t before = fss_extended_list_read_print_at_determine_begin(main, data, at);

      if (before.start <= before.stop) {
        f_print_except_dynamic_partial(data->buffer, before, delimits_object, main->output.to.stream);
      }
    }

    if (data->option & fss_extended_list_read_data_option_trim_d) {
      fl_print_trim_except_dynamic_partial(data->buffer, data->objects.array[at], delimits_object, main->output.to.stream);
    }
    else {
      if (data->buffer.used) {
        f_string_range_t before = f_string_range_t_initialize;
        f_string_range_t after = f_string_range_t_initialize;

        if (data->option & fss_extended_list_read_data_option_original_d) {
          if (data->objects.array[at].start <= data->objects.array[at].stop) {
            if (data->objects.array[at].start) {
              before.start = before.stop = data->objects.array[at].start - 1;

              while (before.start && data->buffer.string[before.start] != f_fss_eol_s.string[0]) --before.start;

              if (data->buffer.string[before.start] == f_fss_eol_s.string[0]) ++before.start;
            }

            if (f_fss_extended_list_open_s.used && data->objects.array[at].stop + 1 < data->buffer.used) {
              after.start = after.stop = data->objects.array[at].stop + 1;

              if (data->buffer.string[after.stop] == f_fss_extended_list_open_s.string[0]) {
                after.start = 1;
                after.stop = 0;
              }
              else {
                while (after.stop < data->buffer.used && data->buffer.string[after.stop] != f_fss_extended_list_open_s.string[0]) ++after.stop;

                if (after.stop == data->buffer.used || data->buffer.string[after.stop] == f_fss_extended_list_open_s.string[0]) --after.stop;
              }
            }
          }
        }

        if (before.start <= before.stop) {
          f_print_except_dynamic_partial(data->buffer, before, delimits_object, main->output.to.stream);
        }

        f_print_except_dynamic_partial(data->buffer, data->objects.array[at], delimits_object, main->output.to.stream);

        if (after.start <= after.stop) {
          f_print_except_dynamic_partial(data->buffer, after, delimits_object, main->output.to.stream);
        }
      }
    }

    fss_extended_list_read_print_object_end(main, data);
  }
#endif // _di_fss_extended_list_read_print_at_object_

#ifndef _di_fss_extended_list_read_print_content_ignore_
  void fss_extended_list_read_print_content_ignore(fll_program_data_t * const main) {

    if (main->parameters.array[fss_extended_list_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_extended_list_read_pipe_content_ignore_s, main->output.to.stream);
    }
  }
#endif // _di_fss_extended_list_read_print_content_ignore_

#ifndef _di_fss_extended_list_read_print_object_end_
  void fss_extended_list_read_print_object_end(fll_program_data_t * const main, fss_extended_list_read_data_t * const data) {

    if (main->parameters.array[fss_extended_list_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_extended_list_read_pipe_content_start_s, main->output.to.stream);
    }
    else {
      if ((data->option & fss_extended_list_read_data_option_object_d) && (data->option & fss_extended_list_read_data_option_content_d)) {
        f_print_dynamic_raw(f_fss_extended_list_open_s, main->output.to.stream);
        f_print_dynamic_raw(f_fss_extended_list_open_end_s, main->output.to.stream);
      }
    }
  }
#endif // _di_fss_extended_list_read_print_object_end_

#ifndef _di_fss_extended_list_read_print_set_end_
  void fss_extended_list_read_print_set_end(fll_program_data_t * const main, fss_extended_list_read_data_t * const data) {

    if (main->parameters.array[fss_extended_list_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_extended_list_read_pipe_content_end_s, main->output.to.stream);
    }
    else {
      if (data->option & fss_extended_list_read_data_option_object_d) {
        if (data->option & fss_extended_list_read_data_option_content_d) {
          f_print_dynamic_raw(f_fss_extended_list_close_s, main->output.to.stream);
          f_print_dynamic_raw(f_fss_extended_list_close_end_s, main->output.to.stream);
        }
        else if (!(data->option & fss_extended_list_read_data_option_content_d)) {
          f_print_dynamic_raw(f_fss_eol_s, main->output.to.stream);
        }
      }
    }
  }
#endif // _di_fss_extended_list_read_print_set_end_

#ifndef _di_fss_extended_list_read_print_one_
  void fss_extended_list_read_print_one(fll_program_data_t * const main) {

    f_print_dynamic_raw(f_string_ascii_1_s, main->output.to.stream);
    f_print_dynamic_raw(f_string_eol_s, main->output.to.stream);
  }
#endif // _di_fss_extended_list_read_print_one_

#ifndef _di_fss_extended_list_read_print_zero_
  void fss_extended_list_read_print_zero(fll_program_data_t * const main) {

    f_print_dynamic_raw(f_string_ascii_0_s, main->output.to.stream);
    f_print_dynamic_raw(f_string_eol_s, main->output.to.stream);
  }
#endif // _di_fss_extended_list_read_print_zero_

#ifdef __cplusplus
} // extern "C"
#endif
