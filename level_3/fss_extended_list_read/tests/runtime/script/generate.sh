#!/bin.bash
# license: lgpl-2.1-or-later
# programmer: Kevin Day
#
# Helper script for manually generating tests.
#
# This is a simple script that only accepts these arguments in this specific order:
#   1) The standard, using the 4-digit standard hexidecimal number (may also prepend "test-" to use 'fss_read -A').
#   2) The source file to read.
#   3) The destination directory to write to.
#
# Tests using the -n/--name will need to be manually created.
#

generate_main() {
  local standard="${1}"
  local file_source="${2}"
  local path_destination="${3}"
  local failure=0
  local test_base=
  local test_current=

  if [[ $standard == "" ]] ; then
    echo "ERROR: No standard specified."

    return 1
  fi

  if [[ $file_source == "" ]] ; then
    echo "ERROR: No source file specified."

    return 1
  fi

  if [[ $path_destination == "" ]] ; then
    echo "ERROR: No destination path specified."

    return 1
  fi

  file_source=$(echo ${file_source} | sed -e 's|^//*|/|' -e 's|//*|/|' -e 's|/*$||')
  path_destination=$(echo ${path_destination} | sed -e 's|^//*|/|' -e 's|/*$|/|')

  if [[ ! -f ${file_source} ]] ; then
    echo "ERROR: The source file '${file_source}' either does not exist or is not a file."

    return 1
  fi

  if [[ ! -d ${path_destination} ]] ; then
    echo "ERROR: The destination path '${path_destination}' either does not exist or is not a directory."

    return 1
  fi

  test_base=$(echo $file_source | sed -e 's|.*/||g' -e 's|\..*$||')
  test_current="${path_destination}${test_base}-"

  if [[ ${1} == "0003" ]] ; then
    generate_operate_0003
  elif [[ ${1} == "test-0003" ]] ; then
    generate_operate_test_standard
  fi

  if [[ ${failure} -eq 1 ]] ; then
    return 1
  fi

  return 0
}

generate_operate_0003() {

  fss_extended_list_read +n -c ${file_source} > ${test_current}content.expect &&
  fss_extended_list_read +n -cO ${file_source} > ${test_current}content-original.expect &&
  fss_extended_list_read +n -ct ${file_source} > ${test_current}content-total.expect &&
  fss_extended_list_read +n -cT ${file_source} > ${test_current}content-trim.expect &&
  fss_extended_list_read +n -cl 0 ${file_source} > ${test_current}content-line-0.expect &&
  fss_extended_list_read +n -cl 1 ${file_source} > ${test_current}content-line-1.expect &&
  fss_extended_list_read +n -cl 5 ${file_source} > ${test_current}content-line-5.expect &&
  fss_extended_list_read +n -cl 6 ${file_source} > ${test_current}content-line-6.expect &&
  fss_extended_list_read +n -cl 100 ${file_source} > ${test_current}content-line-100.expect &&
  fss_extended_list_read +n -clO 0 ${file_source} > ${test_current}content-line-0-original.expect &&
  fss_extended_list_read +n -clO 1 ${file_source} > ${test_current}content-line-1-original.expect &&
  fss_extended_list_read +n -clO 5 ${file_source} > ${test_current}content-line-5-original.expect &&
  fss_extended_list_read +n -clO 6 ${file_source} > ${test_current}content-line-6-original.expect &&
  fss_extended_list_read +n -clO 100 ${file_source} > ${test_current}content-line-100-original.expect &&
  fss_extended_list_read +n -cs 0 ${file_source} > ${test_current}content-select-0.expect &&
  fss_extended_list_read +n -cs 1 ${file_source} > ${test_current}content-select-1.expect &&
  fss_extended_list_read +n -cs 5 ${file_source} > ${test_current}content-select-5.expect &&
  fss_extended_list_read +n -cs 6 ${file_source} > ${test_current}content-select-6.expect &&
  fss_extended_list_read +n -csO 0 ${file_source} > ${test_current}content-select-0-original.expect &&
  fss_extended_list_read +n -csO 1 ${file_source} > ${test_current}content-select-1-original.expect &&
  fss_extended_list_read +n -csO 5 ${file_source} > ${test_current}content-select-5-original.expect &&
  fss_extended_list_read +n -csO 6 ${file_source} > ${test_current}content-select-6-original.expect &&
  fss_extended_list_read +n -csO 100 ${file_source} > ${test_current}content-select-100-original.expect &&
  fss_extended_list_read +n -cs 100 ${file_source} > ${test_current}content-select-100.expect &&
  fss_extended_list_read +n -cC ${file_source} > ${test_current}content-columns.expect &&
  fss_extended_list_read +n -cCe ${file_source} > ${test_current}content-columns-empty.expect &&
  fss_extended_list_read +n -ca 0 ${file_source} > ${test_current}content-at-0.expect &&
  fss_extended_list_read +n -ca 1 ${file_source} > ${test_current}content-at-1.expect &&
  fss_extended_list_read +n -ca 5 ${file_source} > ${test_current}content-at-5.expect &&
  fss_extended_list_read +n -ca 6 ${file_source} > ${test_current}content-at-6.expect &&
  fss_extended_list_read +n -ca 100 ${file_source} > ${test_current}content-at-100.expect &&
  fss_extended_list_read +n -cn hi ${file_source} > ${test_current}content-name-hi.expect &&
  fss_extended_list_read +n -cn payload ${file_source} > ${test_current}content-name-payload.expect &&
  fss_extended_list_read +n -cn a ${file_source} > ${test_current}content-name-a.expect &&
  fss_extended_list_read +n -cnC a ${file_source} > ${test_current}content-name-a-columns.expect &&
  fss_extended_list_read +n -cnCe a ${file_source} > ${test_current}content-name-a-columns-empty.expect &&
  fss_extended_list_read +n -cnO a ${file_source} > ${test_current}content-name-a-original.expect &&
  fss_extended_list_read +n -cnOe a ${file_source} > ${test_current}content-name-a-original-empty.expect &&
  fss_extended_list_read +n -cn "" ${file_source} > ${test_current}content-name-.expect &&
  fss_extended_list_read +n -cn мир ${file_source} > ${test_current}content-name-мир.expect &&
  fss_extended_list_read +n -cn "привет has space" ${file_source} > ${test_current}content-name-привет_has_space.expect &&
  fss_extended_list_read +n -cnt hi ${file_source} > ${test_current}content-name-hi-total.expect &&
  fss_extended_list_read +n -cnt payload ${file_source} > ${test_current}content-name-payload-total.expect &&
  fss_extended_list_read +n -cnt a ${file_source} > ${test_current}content-name-a-total.expect &&
  fss_extended_list_read +n -cnt "" ${file_source} > ${test_current}content-name--total.expect &&
  fss_extended_list_read +n -cnt мир ${file_source} > ${test_current}content-name-мир-total.expect &&
  fss_extended_list_read +n -cnt "привет has space" ${file_source} > ${test_current}content-name-привет_has_space-total.expect &&
  fss_extended_list_read +n -cns hi 0 ${file_source} > ${test_current}content-name-hi-select-0.expect &&
  fss_extended_list_read +n -cns hi 1 ${file_source} > ${test_current}content-name-hi-select-1.expect &&
  fss_extended_list_read +n -cnse hi 0 ${file_source} > ${test_current}content-name-hi-select-0-empty.expect &&
  fss_extended_list_read +n -cnse hi 1 ${file_source} > ${test_current}content-name-hi-select-1-empty.expect &&
  fss_extended_list_read +n -cns payload 0 ${file_source} > ${test_current}content-name-payload-select-0.expect &&
  fss_extended_list_read +n -cns payload 1 ${file_source} > ${test_current}content-name-payload-select-1.expect &&
  fss_extended_list_read +n -cns a 0 ${file_source} > ${test_current}content-name-a-select-0.expect &&
  fss_extended_list_read +n -cns a 1 ${file_source} > ${test_current}content-name-a-select-1.expect &&
  fss_extended_list_read +n -cns a 2 ${file_source} > ${test_current}content-name-a-select-2.expect &&
  fss_extended_list_read +n -cns a 5 ${file_source} > ${test_current}content-name-a-select-5.expect &&
  fss_extended_list_read +n -cns a 6 ${file_source} > ${test_current}content-name-a-select-6.expect &&
  fss_extended_list_read +n -cns a 100 ${file_source} > ${test_current}content-name-a-select-100.expect &&
  fss_extended_list_read +n -cns "" 0 ${file_source} > ${test_current}content-name--select-0.expect &&
  fss_extended_list_read +n -cns "" 1 ${file_source} > ${test_current}content-name--select-1.expect &&
  fss_extended_list_read +n -cns мир 0 ${file_source} > ${test_current}content-name-мир-select-0.expect &&
  fss_extended_list_read +n -cns мир 1 ${file_source} > ${test_current}content-name-мир-select-1.expect &&
  fss_extended_list_read +n -cns "привет has space" 0 ${file_source} > ${test_current}content-name-привет_has_space-select-0.expect &&
  fss_extended_list_read +n -cns "привет has space" 1 ${file_source} > ${test_current}content-name-привет_has_space-select-1.expect &&
  fss_extended_list_read +n -cnset hi 0 ${file_source} > ${test_current}content-name-hi-select-0-empty-total.expect &&
  fss_extended_list_read +n -cnset hi 1 ${file_source} > ${test_current}content-name-hi-select-1-empty-total.expect &&
  fss_extended_list_read +n -cnst hi 0 ${file_source} > ${test_current}content-name-hi-select-0-total.expect &&
  fss_extended_list_read +n -cnst hi 1 ${file_source} > ${test_current}content-name-hi-select-1-total.expect &&
  fss_extended_list_read +n -cnst payload 0 ${file_source} > ${test_current}content-name-payload-select-0-total.expect &&
  fss_extended_list_read +n -cnst payload 1 ${file_source} > ${test_current}content-name-payload-select-1-total.expect &&
  fss_extended_list_read +n -cnst a 0 ${file_source} > ${test_current}content-name-a-select-0-total.expect &&
  fss_extended_list_read +n -cnst a 1 ${file_source} > ${test_current}content-name-a-select-1-total.expect &&
  fss_extended_list_read +n -cnst a 2 ${file_source} > ${test_current}content-name-a-select-2-total.expect &&
  fss_extended_list_read +n -cnst a 5 ${file_source} > ${test_current}content-name-a-select-5-total.expect &&
  fss_extended_list_read +n -cnst a 6 ${file_source} > ${test_current}content-name-a-select-6-total.expect &&
  fss_extended_list_read +n -cnst a 100 ${file_source} > ${test_current}content-name-a-select-100-total.expect &&
  fss_extended_list_read +n -cnst "" 0 ${file_source} > ${test_current}content-name--select-0-total.expect &&
  fss_extended_list_read +n -cnst "" 1 ${file_source} > ${test_current}content-name--select-1-total.expect &&
  fss_extended_list_read +n -cnst мир 0 ${file_source} > ${test_current}content-name-мир-select-0-total.expect &&
  fss_extended_list_read +n -cnst мир 1 ${file_source} > ${test_current}content-name-мир-select-1-total.expect &&
  fss_extended_list_read +n -cnst "привет has space" 0 ${file_source} > ${test_current}content-name-привет_has_space-select-0-total.expect &&
  fss_extended_list_read +n -cnst "привет has space" 1 ${file_source} > ${test_current}content-name-привет_has_space-select-1-total.expect &&

  fss_extended_list_read +n -o ${file_source} > ${test_current}object.expect &&
  fss_extended_list_read +n -oO ${file_source} > ${test_current}object-original.expect &&
  fss_extended_list_read +n -ot ${file_source} > ${test_current}object-total.expect &&
  fss_extended_list_read +n -oT ${file_source} > ${test_current}object-trim.expect &&
  fss_extended_list_read +n -ol 0 ${file_source} > ${test_current}object-line-0.expect &&
  fss_extended_list_read +n -ol 1 ${file_source} > ${test_current}object-line-1.expect &&
  fss_extended_list_read +n -ol 5 ${file_source} > ${test_current}object-line-5.expect &&
  fss_extended_list_read +n -ol 6 ${file_source} > ${test_current}object-line-6.expect &&
  fss_extended_list_read +n -ol 100 ${file_source} > ${test_current}object-line-100.expect &&
  fss_extended_list_read +n -os 0 ${file_source} > ${test_current}object-select-0.expect &&
  fss_extended_list_read +n -os 1 ${file_source} > ${test_current}object-select-1.expect &&
  fss_extended_list_read +n -os 5 ${file_source} > ${test_current}object-select-5.expect &&
  fss_extended_list_read +n -os 6 ${file_source} > ${test_current}object-select-6.expect &&
  fss_extended_list_read +n -os 100 ${file_source} > ${test_current}object-select-100.expect &&
  fss_extended_list_read +n -oC ${file_source} > ${test_current}object-columns.expect &&
  fss_extended_list_read +n -oa 0 ${file_source} > ${test_current}object-at-0.expect &&
  fss_extended_list_read +n -oa 1 ${file_source} > ${test_current}object-at-1.expect &&
  fss_extended_list_read +n -oa 5 ${file_source} > ${test_current}object-at-5.expect &&
  fss_extended_list_read +n -oa 6 ${file_source} > ${test_current}object-at-6.expect &&
  fss_extended_list_read +n -oa 100 ${file_source} > ${test_current}object-at-100.expect &&
  fss_extended_list_read +n -on hi ${file_source} > ${test_current}object-name-hi.expect &&
  fss_extended_list_read +n -on payload ${file_source} > ${test_current}object-name-payload.expect &&
  fss_extended_list_read +n -on "" ${file_source} > ${test_current}object-name-.expect &&
  fss_extended_list_read +n -on мир ${file_source} > ${test_current}object-name-мир.expect &&
  fss_extended_list_read +n -on "привет has space" ${file_source} > ${test_current}object-name-привет_has_space.expect &&
  fss_extended_list_read +n -on a ${file_source} > ${test_current}object-name-a.expect &&
  fss_extended_list_read +n -onC a ${file_source} > ${test_current}object-name-a-columns.expect &&
  fss_extended_list_read +n -onCe a ${file_source} > ${test_current}object-name-a-columns-empty.expect &&
  fss_extended_list_read +n -onO a ${file_source} > ${test_current}object-name-a-original.expect &&
  fss_extended_list_read +n -onOe a ${file_source} > ${test_current}object-name-a-original-empty.expect &&
  fss_extended_list_read +n -ont hi ${file_source} > ${test_current}object-name-hi-total.expect &&
  fss_extended_list_read +n -ont payload ${file_source} > ${test_current}object-name-payload-total.expect &&
  fss_extended_list_read +n -ont a ${file_source} > ${test_current}object-name-a-total.expect &&
  fss_extended_list_read +n -ont "" ${file_source} > ${test_current}object-name--total.expect &&
  fss_extended_list_read +n -ont мир ${file_source} > ${test_current}object-name-мир-total.expect &&
  fss_extended_list_read +n -ont "привет has space" ${file_source} > ${test_current}object-name-привет_has_space-total.expect &&
  fss_extended_list_read +n -ons hi 0 ${file_source} > ${test_current}object-name-hi-select-0.expect &&
  fss_extended_list_read +n -ons hi 1 ${file_source} > ${test_current}object-name-hi-select-1.expect &&
  fss_extended_list_read +n -onse hi 0 ${file_source} > ${test_current}object-name-hi-select-0-empty.expect &&
  fss_extended_list_read +n -onse hi 1 ${file_source} > ${test_current}object-name-hi-select-1-empty.expect &&
  fss_extended_list_read +n -ons payload 0 ${file_source} > ${test_current}object-name-payload-select-0.expect &&
  fss_extended_list_read +n -ons payload 1 ${file_source} > ${test_current}object-name-payload-select-1.expect &&
  fss_extended_list_read +n -ons a 0 ${file_source} > ${test_current}object-name-a-select-0.expect &&
  fss_extended_list_read +n -ons a 1 ${file_source} > ${test_current}object-name-a-select-1.expect &&
  fss_extended_list_read +n -ons a 2 ${file_source} > ${test_current}object-name-a-select-2.expect &&
  fss_extended_list_read +n -ons a 5 ${file_source} > ${test_current}object-name-a-select-5.expect &&
  fss_extended_list_read +n -ons a 6 ${file_source} > ${test_current}object-name-a-select-6.expect &&
  fss_extended_list_read +n -ons a 100 ${file_source} > ${test_current}object-name-a-select-100.expect &&
  fss_extended_list_read +n -ons "" 0 ${file_source} > ${test_current}object-name--select-0.expect &&
  fss_extended_list_read +n -ons "" 1 ${file_source} > ${test_current}object-name--select-1.expect &&
  fss_extended_list_read +n -ons мир 0 ${file_source} > ${test_current}object-name-мир-select-0.expect &&
  fss_extended_list_read +n -ons мир 1 ${file_source} > ${test_current}object-name-мир-select-1.expect &&
  fss_extended_list_read +n -ons привет 0 ${file_source} > ${test_current}object-name-привет-select-0.expect &&
  fss_extended_list_read +n -ons привет 1 ${file_source} > ${test_current}object-name-привет-select-1.expect &&
  fss_extended_list_read +n -onset hi 0 ${file_source} > ${test_current}object-name-hi-select-0-empty-total.expect &&
  fss_extended_list_read +n -onset hi 1 ${file_source} > ${test_current}object-name-hi-select-1-empty-total.expect &&
  fss_extended_list_read +n -onst hi 0 ${file_source} > ${test_current}object-name-hi-select-0-total.expect &&
  fss_extended_list_read +n -onst hi 1 ${file_source} > ${test_current}object-name-hi-select-1-total.expect &&
  fss_extended_list_read +n -onst payload 0 ${file_source} > ${test_current}object-name-payload-select-0-total.expect &&
  fss_extended_list_read +n -onst payload 1 ${file_source} > ${test_current}object-name-payload-select-1-total.expect &&
  fss_extended_list_read +n -onst a 0 ${file_source} > ${test_current}object-name-a-select-0-total.expect &&
  fss_extended_list_read +n -onst a 1 ${file_source} > ${test_current}object-name-a-select-1-total.expect &&
  fss_extended_list_read +n -onst a 2 ${file_source} > ${test_current}object-name-a-select-2-total.expect &&
  fss_extended_list_read +n -onst a 5 ${file_source} > ${test_current}object-name-a-select-5-total.expect &&
  fss_extended_list_read +n -onst a 6 ${file_source} > ${test_current}object-name-a-select-6-total.expect &&
  fss_extended_list_read +n -onst a 100 ${file_source} > ${test_current}object-name-a-select-100-total.expect &&
  fss_extended_list_read +n -onst "" 0 ${file_source} > ${test_current}object-name--select-0-total.expect &&
  fss_extended_list_read +n -onst "" 1 ${file_source} > ${test_current}object-name--select-1-total.expect &&
  fss_extended_list_read +n -onst мир 0 ${file_source} > ${test_current}object-name-мир-select-0-total.expect &&
  fss_extended_list_read +n -onst мир 1 ${file_source} > ${test_current}object-name-мир-select-1-total.expect &&
  fss_extended_list_read +n -onst "привет has space" 0 ${file_source} > ${test_current}object-name-привет_has_space-select-0-total.expect &&
  fss_extended_list_read +n -onst "привет has space" 1 ${file_source} > ${test_current}object-name-привет_has_space-select-1-total.expect &&

  fss_extended_list_read +n -oc ${file_source} > ${test_current}object_and_content.expect &&
  fss_extended_list_read +n -ocO ${file_source} > ${test_current}object_and_content-original.expect &&
  fss_extended_list_read +n -oct ${file_source} > ${test_current}object_and_content-total.expect &&
  fss_extended_list_read +n -ocT ${file_source} > ${test_current}object_and_content-trim.expect &&
  fss_extended_list_read +n -ocl 0 ${file_source} > ${test_current}object_and_content-line-0.expect &&
  fss_extended_list_read +n -ocl 1 ${file_source} > ${test_current}object_and_content-line-1.expect &&
  fss_extended_list_read +n -ocl 5 ${file_source} > ${test_current}object_and_content-line-5.expect &&
  fss_extended_list_read +n -ocl 6 ${file_source} > ${test_current}object_and_content-line-6.expect &&
  fss_extended_list_read +n -ocl 100 ${file_source} > ${test_current}object_and_content-line-100.expect &&
  fss_extended_list_read +n -ocs 0 ${file_source} > ${test_current}object_and_content-select-0.expect &&
  fss_extended_list_read +n -ocs 1 ${file_source} > ${test_current}object_and_content-select-1.expect &&
  fss_extended_list_read +n -ocs 5 ${file_source} > ${test_current}object_and_content-select-5.expect &&
  fss_extended_list_read +n -ocs 6 ${file_source} > ${test_current}object_and_content-select-6.expect &&
  fss_extended_list_read +n -ocs 100 ${file_source} > ${test_current}object_and_content-select-100.expect &&
  fss_extended_list_read +n -ocC ${file_source} > ${test_current}object_and_content-columns.expect &&
  fss_extended_list_read +n -ocCe ${file_source} > ${test_current}object_and_content-columns-empty.expect &&
  fss_extended_list_read +n -oca 0 ${file_source} > ${test_current}object_and_content-at-0.expect &&
  fss_extended_list_read +n -oca 1 ${file_source} > ${test_current}object_and_content-at-1.expect &&
  fss_extended_list_read +n -oca 5 ${file_source} > ${test_current}object_and_content-at-5.expect &&
  fss_extended_list_read +n -oca 6 ${file_source} > ${test_current}object_and_content-at-6.expect &&
  fss_extended_list_read +n -oca 100 ${file_source} > ${test_current}object_and_content-at-100.expect &&
  fss_extended_list_read +n -ocn hi ${file_source} > ${test_current}object_and_content-name-hi.expect &&
  fss_extended_list_read +n -ocn payload ${file_source} > ${test_current}object_and_content-name-payload.expect &&
  fss_extended_list_read +n -ocn a ${file_source} > ${test_current}object_and_content-name-a.expect &&
  fss_extended_list_read +n -ocnC a ${file_source} > ${test_current}object_and_content-name-a-columns.expect &&
  fss_extended_list_read +n -ocnCe a ${file_source} > ${test_current}object_and_content-name-a-columns-empty.expect &&
  fss_extended_list_read +n -ocnO a ${file_source} > ${test_current}object_and_content-name-a-original.expect &&
  fss_extended_list_read +n -ocnOe a ${file_source} > ${test_current}object_and_content-name-a-original-empty.expect &&
  fss_extended_list_read +n -ocn "" ${file_source} > ${test_current}object_and_content-name-.expect &&
  fss_extended_list_read +n -ocn мир ${file_source} > ${test_current}object_and_content-name-мир.expect &&
  fss_extended_list_read +n -ocn привет ${file_source} > ${test_current}object_and_content-name-привет.expect &&
  fss_extended_list_read +n -ocnt hi ${file_source} > ${test_current}object_and_content-name-hi-total.expect &&
  fss_extended_list_read +n -ocnt payload ${file_source} > ${test_current}object_and_content-name-payload-total.expect &&
  fss_extended_list_read +n -ocnt a ${file_source} > ${test_current}object_and_content-name-a-total.expect &&
  fss_extended_list_read +n -ocnt "" ${file_source} > ${test_current}object_and_content-name--total.expect &&
  fss_extended_list_read +n -ocnt мир ${file_source} > ${test_current}object_and_content-name-мир-total.expect &&
  fss_extended_list_read +n -ocnt "привет has space" ${file_source} > ${test_current}object_and_content-name-привет_has_space-total.expect &&
  fss_extended_list_read +n -ocns hi 0 ${file_source} > ${test_current}object_and_content-name-hi-select-0.expect &&
  fss_extended_list_read +n -ocns hi 1 ${file_source} > ${test_current}object_and_content-name-hi-select-1.expect &&
  fss_extended_list_read +n -ocnse hi 0 ${file_source} > ${test_current}object_and_content-name-hi-select-0-empty.expect &&
  fss_extended_list_read +n -ocnse hi 1 ${file_source} > ${test_current}object_and_content-name-hi-select-1-empty.expect &&
  fss_extended_list_read +n -ocns payload 0 ${file_source} > ${test_current}object_and_content-name-payload-select-0.expect &&
  fss_extended_list_read +n -ocns payload 1 ${file_source} > ${test_current}object_and_content-name-payload-select-1.expect &&
  fss_extended_list_read +n -ocns a 0 ${file_source} > ${test_current}object_and_content-name-a-select-0.expect &&
  fss_extended_list_read +n -ocns a 1 ${file_source} > ${test_current}object_and_content-name-a-select-1.expect &&
  fss_extended_list_read +n -ocns a 2 ${file_source} > ${test_current}object_and_content-name-a-select-2.expect &&
  fss_extended_list_read +n -ocns a 5 ${file_source} > ${test_current}object_and_content-name-a-select-5.expect &&
  fss_extended_list_read +n -ocns a 6 ${file_source} > ${test_current}object_and_content-name-a-select-6.expect &&
  fss_extended_list_read +n -ocns a 100 ${file_source} > ${test_current}object_and_content-name-a-select-100.expect &&
  fss_extended_list_read +n -ocns "" 0 ${file_source} > ${test_current}object_and_content-name--select-0.expect &&
  fss_extended_list_read +n -ocns "" 1 ${file_source} > ${test_current}object_and_content-name--select-1.expect &&
  fss_extended_list_read +n -ocns мир 0 ${file_source} > ${test_current}object_and_content-name-мир-select-0.expect &&
  fss_extended_list_read +n -ocns мир 1 ${file_source} > ${test_current}object_and_content-name-мир-select-1.expect &&
  fss_extended_list_read +n -ocns "привет has space" 0 ${file_source} > ${test_current}object_and_content-name-привет_has_space-select-0.expect &&
  fss_extended_list_read +n -ocns "привет has space" 1 ${file_source} > ${test_current}object_and_content-name-привет_has_space-select-1.expect &&
  fss_extended_list_read +n -ocnset hi 0 ${file_source} > ${test_current}object_and_content-name-hi-select-0-empty-total.expect &&
  fss_extended_list_read +n -ocnset hi 1 ${file_source} > ${test_current}object_and_content-name-hi-select-1-empty-total.expect &&
  fss_extended_list_read +n -ocnst hi 0 ${file_source} > ${test_current}object_and_content-name-hi-select-0-total.expect &&
  fss_extended_list_read +n -ocnst hi 1 ${file_source} > ${test_current}object_and_content-name-hi-select-1-total.expect &&
  fss_extended_list_read +n -ocnst payload 0 ${file_source} > ${test_current}object_and_content-name-payload-select-0-total.expect &&
  fss_extended_list_read +n -ocnst payload 1 ${file_source} > ${test_current}object_and_content-name-payload-select-1-total.expect &&
  fss_extended_list_read +n -ocnst a 0 ${file_source} > ${test_current}object_and_content-name-a-select-0-total.expect &&
  fss_extended_list_read +n -ocnst a 1 ${file_source} > ${test_current}object_and_content-name-a-select-1-total.expect &&
  fss_extended_list_read +n -ocnst a 2 ${file_source} > ${test_current}object_and_content-name-a-select-2-total.expect &&
  fss_extended_list_read +n -ocnst a 5 ${file_source} > ${test_current}object_and_content-name-a-select-5-total.expect &&
  fss_extended_list_read +n -ocnst a 6 ${file_source} > ${test_current}object_and_content-name-a-select-6-total.expect &&
  fss_extended_list_read +n -ocnst a 100 ${file_source} > ${test_current}object_and_content-name-a-select-100-total.expect &&
  fss_extended_list_read +n -ocnst "" 0 ${file_source} > ${test_current}object_and_content-name--select-0-total.expect &&
  fss_extended_list_read +n -ocnst "" 1 ${file_source} > ${test_current}object_and_content-name--select-1-total.expect &&
  fss_extended_list_read +n -ocnst мир 0 ${file_source} > ${test_current}object_and_content-name-мир-select-0-total.expect &&
  fss_extended_list_read +n -ocnst мир 1 ${file_source} > ${test_current}object_and_content-name-мир-select-1-total.expect &&
  fss_extended_list_read +n -ocnst "привет has space" 0 ${file_source} > ${test_current}object_and_content-name-привет_has_space-select-0-total.expect &&
  fss_extended_list_read +n -ocnst "привет has space" 1 ${file_source} > ${test_current}object_and_content-name-привет_has_space-select-1-total.expect &&

  echo "Generation Complete"

  if [[ $? -ne 0 ]] ; then
    let failure=0
  fi

  return $failure
}

generate_operate_test_standard() {
  standard=$(echo "${standard}" | sed -e 's|^test-||')

  fss_extended_list_read +n -c ${file_source} > ${test_current}content.expect &&
  fss_extended_list_read +n -cO ${file_source} > ${test_current}content-original.expect &&
  fss_extended_list_read +n -ct ${file_source} > ${test_current}content-total.expect &&
  fss_extended_list_read +n -cT ${file_source} > ${test_current}content-trim.expect &&
  fss_extended_list_read +n -cl 0 ${file_source} > ${test_current}content-line-0.expect &&
  fss_extended_list_read +n -cl 1 ${file_source} > ${test_current}content-line-1.expect &&
  fss_extended_list_read +n -cl 5 ${file_source} > ${test_current}content-line-5.expect &&
  fss_extended_list_read +n -cl 6 ${file_source} > ${test_current}content-line-6.expect &&
  fss_extended_list_read +n -cl 100 ${file_source} > ${test_current}content-line-100.expect &&
  fss_extended_list_read +n -clO 0 ${file_source} > ${test_current}content-line-0-original.expect &&
  fss_extended_list_read +n -clO 1 ${file_source} > ${test_current}content-line-1-original.expect &&
  fss_extended_list_read +n -clO 5 ${file_source} > ${test_current}content-line-5-original.expect &&
  fss_extended_list_read +n -clO 6 ${file_source} > ${test_current}content-line-6-original.expect &&
  fss_extended_list_read +n -clO 100 ${file_source} > ${test_current}content-line-100-original.expect &&
  fss_extended_list_read +n -cs 0 ${file_source} > ${test_current}content-select-0.expect &&
  fss_extended_list_read +n -cs 1 ${file_source} > ${test_current}content-select-1.expect &&
  fss_extended_list_read +n -cs 5 ${file_source} > ${test_current}content-select-5.expect &&
  fss_extended_list_read +n -cs 6 ${file_source} > ${test_current}content-select-6.expect &&
  fss_extended_list_read +n -cs 100 ${file_source} > ${test_current}content-select-100.expect &&
  fss_extended_list_read +n -csO 0 ${file_source} > ${test_current}content-select-0-original.expect &&
  fss_extended_list_read +n -csO 1 ${file_source} > ${test_current}content-select-1-original.expect &&
  fss_extended_list_read +n -csO 5 ${file_source} > ${test_current}content-select-5-original.expect &&
  fss_extended_list_read +n -csO 6 ${file_source} > ${test_current}content-select-6-original.expect &&
  fss_extended_list_read +n -csO 100 ${file_source} > ${test_current}content-select-100-original.expect &&
  fss_extended_list_read +n -cC ${file_source} > ${test_current}content-columns.expect &&
  fss_extended_list_read +n -cCe ${file_source} > ${test_current}content-columns-empty.expect &&
  fss_extended_list_read +n -ca 0 ${file_source} > ${test_current}content-at-0.expect &&
  fss_extended_list_read +n -ca 1 ${file_source} > ${test_current}content-at-1.expect &&
  fss_extended_list_read +n -ca 5 ${file_source} > ${test_current}content-at-5.expect &&
  fss_extended_list_read +n -ca 6 ${file_source} > ${test_current}content-at-6.expect &&
  fss_extended_list_read +n -ca 100 ${file_source} > ${test_current}content-at-100.expect &&
  fss_extended_list_read +n -cn hi ${file_source} > ${test_current}content-name-hi.expect &&
  fss_extended_list_read +n -cn payload ${file_source} > ${test_current}content-name-payload.expect &&
  fss_extended_list_read +n -cn a ${file_source} > ${test_current}content-name-a.expect &&
  fss_extended_list_read +n -cnC a ${file_source} > ${test_current}content-name-a-columns.expect &&
  fss_extended_list_read +n -cnCe a ${file_source} > ${test_current}content-name-a-columns-empty.expect &&
  fss_extended_list_read +n -cnO a ${file_source} > ${test_current}content-name-a-original.expect &&
  fss_extended_list_read +n -cnOe a ${file_source} > ${test_current}content-name-a-original-empty.expect &&
  fss_extended_list_read +n -cn "" ${file_source} > ${test_current}content-name-.expect &&
  fss_extended_list_read +n -cn мир ${file_source} > ${test_current}content-name-мир.expect &&
  fss_extended_list_read +n -cn "привет has space" ${file_source} > ${test_current}content-name-привет_has_space.expect &&
  fss_extended_list_read +n -cnt hi ${file_source} > ${test_current}content-name-hi-total.expect &&
  fss_extended_list_read +n -cnt payload ${file_source} > ${test_current}content-name-payload-total.expect &&
  fss_extended_list_read +n -cnt a ${file_source} > ${test_current}content-name-a-total.expect &&
  fss_extended_list_read +n -cnt "" ${file_source} > ${test_current}content-name--total.expect &&
  fss_extended_list_read +n -cnt мир ${file_source} > ${test_current}content-name-мир-total.expect &&
  fss_extended_list_read +n -cnt "привет has space" ${file_source} > ${test_current}content-name-привет_has_space-total.expect &&
  fss_extended_list_read +n -cns hi 0 ${file_source} > ${test_current}content-name-hi-select-0.expect &&
  fss_extended_list_read +n -cns hi 1 ${file_source} > ${test_current}content-name-hi-select-1.expect &&
  fss_extended_list_read +n -cnse hi 0 ${file_source} > ${test_current}content-name-hi-select-0-empty.expect &&
  fss_extended_list_read +n -cnse hi 1 ${file_source} > ${test_current}content-name-hi-select-1-empty.expect &&
  fss_extended_list_read +n -cns payload 0 ${file_source} > ${test_current}content-name-payload-select-0.expect &&
  fss_extended_list_read +n -cns payload 1 ${file_source} > ${test_current}content-name-payload-select-1.expect &&
  fss_extended_list_read +n -cns a 0 ${file_source} > ${test_current}content-name-a-select-0.expect &&
  fss_extended_list_read +n -cns a 1 ${file_source} > ${test_current}content-name-a-select-1.expect &&
  fss_extended_list_read +n -cns a 2 ${file_source} > ${test_current}content-name-a-select-2.expect &&
  fss_extended_list_read +n -cns a 5 ${file_source} > ${test_current}content-name-a-select-5.expect &&
  fss_extended_list_read +n -cns a 6 ${file_source} > ${test_current}content-name-a-select-6.expect &&
  fss_extended_list_read +n -cns a 100 ${file_source} > ${test_current}content-name-a-select-100.expect &&
  fss_extended_list_read +n -cns "" 0 ${file_source} > ${test_current}content-name--select-0.expect &&
  fss_extended_list_read +n -cns "" 1 ${file_source} > ${test_current}content-name--select-1.expect &&
  fss_extended_list_read +n -cns мир 0 ${file_source} > ${test_current}content-name-мир-select-0.expect &&
  fss_extended_list_read +n -cns мир 1 ${file_source} > ${test_current}content-name-мир-select-1.expect &&
  fss_extended_list_read +n -cns "привет has space" 0 ${file_source} > ${test_current}content-name-привет_has_space-select-0.expect &&
  fss_extended_list_read +n -cns "привет has space" 1 ${file_source} > ${test_current}content-name-привет_has_space-select-1.expect &&
  fss_extended_list_read +n -cnset hi 0 ${file_source} > ${test_current}content-name-hi-select-0-empty-total.expect &&
  fss_extended_list_read +n -cnset hi 1 ${file_source} > ${test_current}content-name-hi-select-1-empty-total.expect &&
  fss_extended_list_read +n -cnst hi 0 ${file_source} > ${test_current}content-name-hi-select-0-total.expect &&
  fss_extended_list_read +n -cnst hi 1 ${file_source} > ${test_current}content-name-hi-select-1-total.expect &&
  fss_extended_list_read +n -cnst payload 0 ${file_source} > ${test_current}content-name-payload-select-0-total.expect &&
  fss_extended_list_read +n -cnst payload 1 ${file_source} > ${test_current}content-name-payload-select-1-total.expect &&
  fss_extended_list_read +n -cnst a 0 ${file_source} > ${test_current}content-name-a-select-0-total.expect &&
  fss_extended_list_read +n -cnst a 1 ${file_source} > ${test_current}content-name-a-select-1-total.expect &&
  fss_extended_list_read +n -cnst a 2 ${file_source} > ${test_current}content-name-a-select-2-total.expect &&
  fss_extended_list_read +n -cnst a 5 ${file_source} > ${test_current}content-name-a-select-5-total.expect &&
  fss_extended_list_read +n -cnst a 6 ${file_source} > ${test_current}content-name-a-select-6-total.expect &&
  fss_extended_list_read +n -cnst a 100 ${file_source} > ${test_current}content-name-a-select-100-total.expect &&
  fss_extended_list_read +n -cnst "" 0 ${file_source} > ${test_current}content-name--select-0-total.expect &&
  fss_extended_list_read +n -cnst "" 1 ${file_source} > ${test_current}content-name--select-1-total.expect &&
  fss_extended_list_read +n -cnst мир 0 ${file_source} > ${test_current}content-name-мир-select-0-total.expect &&
  fss_extended_list_read +n -cnst мир 1 ${file_source} > ${test_current}content-name-мир-select-1-total.expect &&
  fss_extended_list_read +n -cnst "привет has space" 0 ${file_source} > ${test_current}content-name-привет_has_space-select-0-total.expect &&
  fss_extended_list_read +n -cnst "привет has space" 1 ${file_source} > ${test_current}content-name-привет_has_space-select-1-total.expect &&

  fss_extended_list_read +n -o ${file_source} > ${test_current}object.expect &&
  fss_extended_list_read +n -oO ${file_source} > ${test_current}object-original.expect &&
  fss_extended_list_read +n -ot ${file_source} > ${test_current}object-total.expect &&
  fss_extended_list_read +n -oT ${file_source} > ${test_current}object-trim.expect &&
  fss_extended_list_read +n -ol 0 ${file_source} > ${test_current}object-line-0.expect &&
  fss_extended_list_read +n -ol 1 ${file_source} > ${test_current}object-line-1.expect &&
  fss_extended_list_read +n -ol 5 ${file_source} > ${test_current}object-line-5.expect &&
  fss_extended_list_read +n -ol 6 ${file_source} > ${test_current}object-line-6.expect &&
  fss_extended_list_read +n -ol 100 ${file_source} > ${test_current}object-line-100.expect &&
  fss_extended_list_read +n -os 0 ${file_source} > ${test_current}object-select-0.expect &&
  fss_extended_list_read +n -os 1 ${file_source} > ${test_current}object-select-1.expect &&
  fss_extended_list_read +n -os 5 ${file_source} > ${test_current}object-select-5.expect &&
  fss_extended_list_read +n -os 6 ${file_source} > ${test_current}object-select-6.expect &&
  fss_extended_list_read +n -os 100 ${file_source} > ${test_current}object-select-100.expect &&
  fss_extended_list_read +n -oC ${file_source} > ${test_current}object-columns.expect &&
  fss_extended_list_read +n -oa 0 ${file_source} > ${test_current}object-at-0.expect &&
  fss_extended_list_read +n -oa 1 ${file_source} > ${test_current}object-at-1.expect &&
  fss_extended_list_read +n -oa 5 ${file_source} > ${test_current}object-at-5.expect &&
  fss_extended_list_read +n -oa 6 ${file_source} > ${test_current}object-at-6.expect &&
  fss_extended_list_read +n -oa 100 ${file_source} > ${test_current}object-at-100.expect &&
  fss_extended_list_read +n -on hi ${file_source} > ${test_current}object-name-hi.expect &&
  fss_extended_list_read +n -on payload ${file_source} > ${test_current}object-name-payload.expect &&
  fss_extended_list_read +n -on "" ${file_source} > ${test_current}object-name-.expect &&
  fss_extended_list_read +n -on мир ${file_source} > ${test_current}object-name-мир.expect &&
  fss_extended_list_read +n -on "привет has space" ${file_source} > ${test_current}object-name-привет_has_space.expect &&
  fss_extended_list_read +n -on a ${file_source} > ${test_current}object-name-a.expect &&
  fss_extended_list_read +n -onC a ${file_source} > ${test_current}object-name-a-columns.expect &&
  fss_extended_list_read +n -onCe a ${file_source} > ${test_current}object-name-a-columns-empty.expect &&
  fss_extended_list_read +n -onO a ${file_source} > ${test_current}object-name-a-original.expect &&
  fss_extended_list_read +n -onOe a ${file_source} > ${test_current}object-name-a-original-empty.expect &&
  fss_extended_list_read +n -ont hi ${file_source} > ${test_current}object-name-hi-total.expect &&
  fss_extended_list_read +n -ont hi ${file_source} > ${test_current}object-name-hi-total.expect &&
  fss_extended_list_read +n -ont payload ${file_source} > ${test_current}object-name-payload-total.expect &&
  fss_extended_list_read +n -ont payload ${file_source} > ${test_current}object-name-payload-total.expect &&
  fss_extended_list_read +n -ont a ${file_source} > ${test_current}object-name-a-total.expect &&
  fss_extended_list_read +n -ont "" ${file_source} > ${test_current}object-name--total.expect &&
  fss_extended_list_read +n -ont мир ${file_source} > ${test_current}object-name-мир-total.expect &&
  fss_extended_list_read +n -ont "привет has space" ${file_source} > ${test_current}object-name-привет_has_space-total.expect &&
  fss_extended_list_read +n -ons hi 0 ${file_source} > ${test_current}object-name-hi-select-0.expect &&
  fss_extended_list_read +n -ons hi 1 ${file_source} > ${test_current}object-name-hi-select-1.expect &&
  fss_extended_list_read +n -onse hi 0 ${file_source} > ${test_current}object-name-hi-select-0-empty.expect &&
  fss_extended_list_read +n -onse hi 1 ${file_source} > ${test_current}object-name-hi-select-1-empty.expect &&
  fss_extended_list_read +n -ons payload 0 ${file_source} > ${test_current}object-name-payload-select-0.expect &&
  fss_extended_list_read +n -ons payload 1 ${file_source} > ${test_current}object-name-payload-select-1.expect &&
  fss_extended_list_read +n -ons a 0 ${file_source} > ${test_current}object-name-a-select-0.expect &&
  fss_extended_list_read +n -ons a 1 ${file_source} > ${test_current}object-name-a-select-1.expect &&
  fss_extended_list_read +n -ons a 2 ${file_source} > ${test_current}object-name-a-select-2.expect &&
  fss_extended_list_read +n -ons a 5 ${file_source} > ${test_current}object-name-a-select-5.expect &&
  fss_extended_list_read +n -ons a 6 ${file_source} > ${test_current}object-name-a-select-6.expect &&
  fss_extended_list_read +n -ons a 100 ${file_source} > ${test_current}object-name-a-select-100.expect &&
  fss_extended_list_read +n -ons "" 0 ${file_source} > ${test_current}object-name--select-0.expect &&
  fss_extended_list_read +n -ons "" 1 ${file_source} > ${test_current}object-name--select-1.expect &&
  fss_extended_list_read +n -ons мир 0 ${file_source} > ${test_current}object-name-мир-select-0.expect &&
  fss_extended_list_read +n -ons мир 1 ${file_source} > ${test_current}object-name-мир-select-1.expect &&
  fss_extended_list_read +n -ons привет 0 ${file_source} > ${test_current}object-name-привет_has_space-select-0.expect &&
  fss_extended_list_read +n -ons привет 1 ${file_source} > ${test_current}object-name-привет_has_space-select-1.expect &&
  fss_extended_list_read +n -onset hi 0 ${file_source} > ${test_current}object-name-hi-select-0-empty-total.expect &&
  fss_extended_list_read +n -onset hi 1 ${file_source} > ${test_current}object-name-hi-select-1-empty-total.expect &&
  fss_extended_list_read +n -onst hi 0 ${file_source} > ${test_current}object-name-hi-select-0-total.expect &&
  fss_extended_list_read +n -onst hi 1 ${file_source} > ${test_current}object-name-hi-select-1-total.expect &&
  fss_extended_list_read +n -onst payload 0 ${file_source} > ${test_current}object-name-payload-select-0-total.expect &&
  fss_extended_list_read +n -onst payload 1 ${file_source} > ${test_current}object-name-payload-select-1-total.expect &&
  fss_extended_list_read +n -onst a 0 ${file_source} > ${test_current}object-name-a-select-0-total.expect &&
  fss_extended_list_read +n -onst a 1 ${file_source} > ${test_current}object-name-a-select-1-total.expect &&
  fss_extended_list_read +n -onst a 2 ${file_source} > ${test_current}object-name-a-select-2-total.expect &&
  fss_extended_list_read +n -onst a 5 ${file_source} > ${test_current}object-name-a-select-5-total.expect &&
  fss_extended_list_read +n -onst a 6 ${file_source} > ${test_current}object-name-a-select-6-total.expect &&
  fss_extended_list_read +n -onst a 100 ${file_source} > ${test_current}object-name-a-select-100-total.expect &&
  fss_extended_list_read +n -onst "" 0 ${file_source} > ${test_current}object-name--select-0-total.expect &&
  fss_extended_list_read +n -onst "" 1 ${file_source} > ${test_current}object-name--select-1-total.expect &&
  fss_extended_list_read +n -onst мир 0 ${file_source} > ${test_current}object-name-мир-select-0-total.expect &&
  fss_extended_list_read +n -onst мир 1 ${file_source} > ${test_current}object-name-мир-select-1-total.expect &&
  fss_extended_list_read +n -onst "привет has space" 0 ${file_source} > ${test_current}object-name-привет_has_space-select-0-total.expect &&
  fss_extended_list_read +n -onst "привет has space" 1 ${file_source} > ${test_current}object-name-привет_has_space-select-1-total.expect &&

  fss_extended_list_read +n -oc ${file_source} > ${test_current}object_and_content.expect &&
  fss_extended_list_read +n -ocO ${file_source} > ${test_current}object_and_content-original.expect &&
  fss_extended_list_read +n -oct ${file_source} > ${test_current}object_and_content-total.expect &&
  fss_extended_list_read +n -ocT ${file_source} > ${test_current}object_and_content-trim.expect &&
  fss_extended_list_read +n -ocl 0 ${file_source} > ${test_current}object_and_content-line-0.expect &&
  fss_extended_list_read +n -ocl 1 ${file_source} > ${test_current}object_and_content-line-1.expect &&
  fss_extended_list_read +n -ocl 5 ${file_source} > ${test_current}object_and_content-line-5.expect &&
  fss_extended_list_read +n -ocl 6 ${file_source} > ${test_current}object_and_content-line-6.expect &&
  fss_extended_list_read +n -ocl 100 ${file_source} > ${test_current}object_and_content-line-100.expect &&
  fss_extended_list_read +n -ocs 0 ${file_source} > ${test_current}object_and_content-select-0.expect &&
  fss_extended_list_read +n -ocs 1 ${file_source} > ${test_current}object_and_content-select-1.expect &&
  fss_extended_list_read +n -ocs 5 ${file_source} > ${test_current}object_and_content-select-5.expect &&
  fss_extended_list_read +n -ocs 6 ${file_source} > ${test_current}object_and_content-select-6.expect &&
  fss_extended_list_read +n -ocs 100 ${file_source} > ${test_current}object_and_content-select-100.expect &&
  fss_extended_list_read +n -ocC ${file_source} > ${test_current}object_and_content-columns.expect &&
  fss_extended_list_read +n -ocCe ${file_source} > ${test_current}object_and_content-columns-empty.expect &&
  fss_extended_list_read +n -oca 0 ${file_source} > ${test_current}object_and_content-at-0.expect &&
  fss_extended_list_read +n -oca 1 ${file_source} > ${test_current}object_and_content-at-1.expect &&
  fss_extended_list_read +n -oca 5 ${file_source} > ${test_current}object_and_content-at-5.expect &&
  fss_extended_list_read +n -oca 6 ${file_source} > ${test_current}object_and_content-at-6.expect &&
  fss_extended_list_read +n -oca 100 ${file_source} > ${test_current}object_and_content-at-100.expect &&
  fss_extended_list_read +n -ocn hi ${file_source} > ${test_current}object_and_content-name-hi.expect &&
  fss_extended_list_read +n -ocn payload ${file_source} > ${test_current}object_and_content-name-payload.expect &&
  fss_extended_list_read +n -ocn a ${file_source} > ${test_current}object_and_content-name-a.expect &&
  fss_extended_list_read +n -ocnC a ${file_source} > ${test_current}object_and_content-name-a-columns.expect &&
  fss_extended_list_read +n -ocnCe a ${file_source} > ${test_current}object_and_content-name-a-columns-empty.expect &&
  fss_extended_list_read +n -ocnO a ${file_source} > ${test_current}object_and_content-name-a-original.expect &&
  fss_extended_list_read +n -ocnOe a ${file_source} > ${test_current}object_and_content-name-a-original-empty.expect &&
  fss_extended_list_read +n -ocn "" ${file_source} > ${test_current}object_and_content-name-.expect &&
  fss_extended_list_read +n -ocn мир ${file_source} > ${test_current}object_and_content-name-мир.expect &&
  fss_extended_list_read +n -ocn привет ${file_source} > ${test_current}object_and_content-name-привет.expect &&
  fss_extended_list_read +n -ocnt hi ${file_source} > ${test_current}object_and_content-name-hi-total.expect &&
  fss_extended_list_read +n -ocnt payload ${file_source} > ${test_current}object_and_content-name-payload-total.expect &&
  fss_extended_list_read +n -ocnt a ${file_source} > ${test_current}object_and_content-name-a-total.expect &&
  fss_extended_list_read +n -ocnt "" ${file_source} > ${test_current}object_and_content-name--total.expect &&
  fss_extended_list_read +n -ocnt мир ${file_source} > ${test_current}object_and_content-name-мир-total.expect &&
  fss_extended_list_read +n -ocnt "привет has space" ${file_source} > ${test_current}object_and_content-name-привет_has_space-total.expect &&
  fss_extended_list_read +n -ocns hi 0 ${file_source} > ${test_current}object_and_content-name-hi-select-0.expect &&
  fss_extended_list_read +n -ocns hi 1 ${file_source} > ${test_current}object_and_content-name-hi-select-1.expect &&
  fss_extended_list_read +n -ocnse hi 0 ${file_source} > ${test_current}object_and_content-name-hi-select-0-empty.expect &&
  fss_extended_list_read +n -ocnse hi 1 ${file_source} > ${test_current}object_and_content-name-hi-select-1-empty.expect &&
  fss_extended_list_read +n -ocns payload 0 ${file_source} > ${test_current}object_and_content-name-payload-select-0.expect &&
  fss_extended_list_read +n -ocns payload 1 ${file_source} > ${test_current}object_and_content-name-payload-select-1.expect &&
  fss_extended_list_read +n -ocns a 0 ${file_source} > ${test_current}object_and_content-name-a-select-0.expect &&
  fss_extended_list_read +n -ocns a 1 ${file_source} > ${test_current}object_and_content-name-a-select-1.expect &&
  fss_extended_list_read +n -ocns a 2 ${file_source} > ${test_current}object_and_content-name-a-select-2.expect &&
  fss_extended_list_read +n -ocns a 5 ${file_source} > ${test_current}object_and_content-name-a-select-5.expect &&
  fss_extended_list_read +n -ocns a 6 ${file_source} > ${test_current}object_and_content-name-a-select-6.expect &&
  fss_extended_list_read +n -ocns a 100 ${file_source} > ${test_current}object_and_content-name-a-select-100.expect &&
  fss_extended_list_read +n -ocns "" 0 ${file_source} > ${test_current}object_and_content-name--select-0.expect &&
  fss_extended_list_read +n -ocns "" 1 ${file_source} > ${test_current}object_and_content-name--select-1.expect &&
  fss_extended_list_read +n -ocns мир 0 ${file_source} > ${test_current}object_and_content-name-мир-select-0.expect &&
  fss_extended_list_read +n -ocns мир 1 ${file_source} > ${test_current}object_and_content-name-мир-select-1.expect &&
  fss_extended_list_read +n -ocns "привет has space" 0 ${file_source} > ${test_current}object_and_content-name-привет_has_space-select-0.expect &&
  fss_extended_list_read +n -ocns "привет has space" 1 ${file_source} > ${test_current}object_and_content-name-привет_has_space-select-1.expect &&
  fss_extended_list_read +n -ocnset hi 0 ${file_source} > ${test_current}object_and_content-name-hi-select-0-empty-total.expect &&
  fss_extended_list_read +n -ocnset hi 1 ${file_source} > ${test_current}object_and_content-name-hi-select-1-empty-total.expect &&
  fss_extended_list_read +n -ocnst hi 0 ${file_source} > ${test_current}object_and_content-name-hi-select-0-total.expect &&
  fss_extended_list_read +n -ocnst hi 1 ${file_source} > ${test_current}object_and_content-name-hi-select-1-total.expect &&
  fss_extended_list_read +n -ocnst payload 0 ${file_source} > ${test_current}object_and_content-name-payload-select-0-total.expect &&
  fss_extended_list_read +n -ocnst payload 1 ${file_source} > ${test_current}object_and_content-name-payload-select-1-total.expect &&
  fss_extended_list_read +n -ocnst a 0 ${file_source} > ${test_current}object_and_content-name-a-select-0-total.expect &&
  fss_extended_list_read +n -ocnst a 1 ${file_source} > ${test_current}object_and_content-name-a-select-1-total.expect &&
  fss_extended_list_read +n -ocnst a 2 ${file_source} > ${test_current}object_and_content-name-a-select-2-total.expect &&
  fss_extended_list_read +n -ocnst a 5 ${file_source} > ${test_current}object_and_content-name-a-select-5-total.expect &&
  fss_extended_list_read +n -ocnst a 6 ${file_source} > ${test_current}object_and_content-name-a-select-6-total.expect &&
  fss_extended_list_read +n -ocnst a 100 ${file_source} > ${test_current}object_and_content-name-a-select-100-total.expect &&
  fss_extended_list_read +n -ocnst "" 0 ${file_source} > ${test_current}object_and_content-name--select-0-total.expect &&
  fss_extended_list_read +n -ocnst "" 1 ${file_source} > ${test_current}object_and_content-name--select-1-total.expect &&
  fss_extended_list_read +n -ocnst мир 0 ${file_source} > ${test_current}object_and_content-name-мир-select-0-total.expect &&
  fss_extended_list_read +n -ocnst мир 1 ${file_source} > ${test_current}object_and_content-name-мир-select-1-total.expect &&
  fss_extended_list_read +n -ocnst "привет has space" 0 ${file_source} > ${test_current}object_and_content-name-привет_has_space-select-0-total.expect &&
  fss_extended_list_read +n -ocnst "привет has space" 1 ${file_source} > ${test_current}object_and_content-name-привет_has_space-select-1-total.expect &&

  echo "Generation Complete"

  if [[ $? -ne 0 ]] ; then
    let failure=0
  fi

  return $failure
}

generate_cleanup() {

  unset generate_main
  unset generate_operate_0003
  unset generate_operate_test_standard
  unset generate_cleanup
}

generate_main "$@"
