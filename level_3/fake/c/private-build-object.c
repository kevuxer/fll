#include "fake.h"
#include "private-common.h"
#include "private-fake.h"
#include "private-build.h"
#include "private-build-object.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_fake_build_object_script_
  int fake_build_object_script(fake_data_t * const data, fake_build_data_t * const data_build, const f_mode_t mode, const f_string_static_t file_stage, f_status_t * const status) {

    if (*status == F_child) return data->main->child;
    if (F_status_is_error(*status) || f_file_exists(file_stage, F_true) == F_true) return 0;

    fake_build_touch(data, file_stage, status);

    return 0;
  }
#endif // _di_fake_build_object_script_

#ifndef _di_fake_build_object_
  int fake_build_object(fake_data_t * const data, fake_build_data_t * const data_build, const f_mode_t mode, const f_string_static_t file_stage, const uint8_t shared, f_status_t * const status) {

    if (*status == F_child) return data->main->child;
    if (F_status_is_error(*status) || f_file_exists(file_stage, F_true) == F_true) return 0;
    if (!data_build->setting.build_sources_object.used && (shared && !data_build->setting.build_sources_object_shared.used || !shared && !data_build->setting.build_sources_object_static.used)) return 0;

    if (data->main->output.verbosity != f_console_verbosity_quiet_e && data->main->output.verbosity != f_console_verbosity_error_e) {
      if (shared) {
        fll_print_format("%r%[Compiling shared object.%]%r", data->main->output.to.stream, f_string_eol_s, data->main->context.set.important, data->main->context.set.important, f_string_eol_s);
      }
      else {
        fll_print_format("%r%[Compiling static object.%]%r", data->main->output.to.stream, f_string_eol_s, data->main->context.set.important, data->main->context.set.important, f_string_eol_s);
      }
    }

    int result = 0;
    int result_final = 0;

    f_number_unsigned_t i = 0;
    f_number_unsigned_t j = 0;
    f_number_unsigned_t k = 0;
    uint8_t v = 0;

    const f_string_dynamics_t *sources[2] = {
      &data_build->setting.build_sources_object,
      shared ? &data_build->setting.build_sources_object_shared : &data_build->setting.build_sources_object_static,
    };

    f_string_dynamics_t arguments = f_string_dynamics_t_initialize;
    f_string_dynamic_t cache_1 = f_string_dynamic_t_initialize;
    f_string_dynamic_t cache_2 = f_string_dynamic_t_initialize;
    f_string_dynamic_t cache_3 = f_string_dynamic_t_initialize;

    for (i = 0; i < 2; ++i) {

      for (j = 0; j < sources[i]->used && F_status_is_error_not(*status); ++j) {

        while (arguments.used) {
          arguments.array[--arguments.used].used = 0;
        } // while

        {
          f_string_statics_t * const values[] = {
            &data_build->setting.build_compiler_arguments,
            shared ? &data_build->setting.build_compiler_arguments_shared : &data_build->setting.build_compiler_arguments_static,
            &data_build->setting.build_compiler_arguments_object,
            shared ? &data_build->setting.build_compiler_arguments_object_shared : &data_build->setting.build_compiler_arguments_object_static,
          };

          for (v = 0; v < 4; ++v) {

            for (k = 0; k < values[v]->used; ++k) {

              if (!values[v]->array[k].used) continue;

              *status = fll_execute_arguments_add(values[v]->array[k], &arguments);
              if (F_status_is_error(*status)) break;
            } // for

            if (F_status_is_error(*status)) {
              fll_error_print(data->main->error, F_status_set_fine(*status), "fll_execute_arguments_add", F_true);

              break;
            }
          } // for
        }

        *status = fake_build_sources_object_add(data, data_build, &sources[i]->array[j], &arguments);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_sources_object_add", F_true);

          break;
        }

        cache_1.used = 0;
        cache_2.used = 0;
        cache_3.used = 0;

        fake_build_get_file_name_without_extension(data, sources[i]->array[j], &cache_1);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_get_file_name_without_extension", F_true);

          break;
        }

        *status = f_file_name_directory(sources[i]->array[j], &cache_2);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "f_file_name_directory", F_true);

          break;
        }

        *status = f_string_dynamic_append_assure(f_path_separator_s, &cache_2);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "f_string_dynamic_append_assure", F_true);

          break;
        }

        *status = f_string_dynamic_append_nulless(shared ? data->path_build_objects_shared : data->path_build_objects_static, &cache_3);

        if (F_status_is_error_not(*status)) {
          *status = f_string_dynamic_append_nulless(cache_2, &cache_3);
        }

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "f_string_dynamic_append_nulless", F_true);

          break;
        }

        *status = f_directory_exists(cache_3);

        if (*status == F_false) {
          fll_error_file_print(data->main->error, F_file_type_not_directory, "f_directory_exists", F_true, cache_3, f_file_operation_find_s, fll_error_file_type_directory_e);

          *status = F_status_set_error(F_failure);

          break;
        }

        if (*status == F_file_found_not) {
          *status = f_directory_create(cache_3, mode.directory);

          if (F_status_is_error(*status)) {
            if (F_status_set_fine(*status) == F_file_found_not) {
              flockfile(data->main->error.to.stream);

              fl_print_format("%[%QThe path '%]", data->main->error.to.stream, data->main->error.context, data->main->error.prefix, data->main->error.context);
              fl_print_format("%[%Q%]", data->main->error.to.stream, data->main->error.notable, cache_3, data->main->error.notable);
              fl_print_format("%[' could not be created, a parent directory is invalid or does not exist.%]%r", data->main->error.to.stream, data->main->error.context, data->main->error.context, f_string_eol_s);

              funlockfile(data->main->error.to.stream);
            }
            else {
              fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_directory_create", F_true, cache_3, f_file_operation_create_s, fll_error_file_type_directory_e);
            }

            break;
          }
        }
        else if (F_status_is_error(*status)) {
          fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_directory_exists", F_true, cache_3, f_file_operation_create_s, fll_error_file_type_directory_e);

          break;
        }

        if (F_status_is_error_not(*status)) {
          *status = f_string_dynamic_append_nulless(cache_1, &cache_3);
        }

        if (F_status_is_error_not(*status)) {
          *status = f_string_dynamic_append_nulless(fake_build_parameter_object_name_suffix_s, &cache_3);
        }

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "f_string_dynamic_append_nulless", F_true);

          break;
        }

        {
          const f_string_static_t values[] = {
            shared ? f_string_empty_s : fake_build_parameter_library_static_s,
            fake_build_parameter_object_compile_s,
            fake_build_parameter_object_output_s,
            cache_3,
          };

          for (v = 0; v < 4; ++v) {

            if (!values[v].used) continue;

            *status = fll_execute_arguments_add(values[v], &arguments);

            if (F_status_is_error(*status)) {
              fll_error_print(data->main->error, F_status_set_fine(*status), "fll_execute_arguments_add", F_true);

              break;
            }
          } // for
        }

        fake_build_arguments_standard_add(data, data_build, F_true, fake_build_type_object_e, &arguments, status);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_arguments_standard_add", F_true);

          break;
        }

        result = fake_execute(data, data_build->environment, data_build->setting.build_compiler, arguments, status);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fake_execute", F_true);

          break;
        }

        if (result && !result_final) {
          result_final = result;
        }
      } // for
    } // for

    macro_f_string_dynamic_t_delete_simple(cache_1);
    macro_f_string_dynamic_t_delete_simple(cache_2);
    macro_f_string_dynamic_t_delete_simple(cache_3);
    macro_f_string_dynamics_t_delete_simple(arguments);

    if (F_status_is_error_not(*status) && *status != F_child) {
      fake_build_touch(data, file_stage, status);
    }

    return result_final;
  }
#endif // _di_fake_build_object_

#ifdef __cplusplus
} // extern "C"
#endif
