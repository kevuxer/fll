#include "fake.h"
#include "private-common.h"
#include "private-fake.h"
#include "private-build.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_fake_build_library_script_
  int fake_build_library_script(fake_data_t * const data, fake_build_data_t * const data_build, const f_mode_t mode, const f_string_static_t file_stage, f_status_t * const status) {

    if (*status == F_child) return data->main->child;
    if (F_status_is_error(*status) || f_file_exists(file_stage, F_true) == F_true) return 0;

    fake_build_touch(data, file_stage, status);

    return 0;
  }
#endif // _di_fake_build_library_script_

#ifndef _di_fake_build_library_shared_
  int fake_build_library_shared(fake_data_t * const data, fake_build_data_t * const data_build, const f_mode_t mode, const f_string_static_t file_stage, f_status_t * const status) {

    if (*status == F_child) return data->main->child;
    if (F_status_is_error(*status) || f_file_exists(file_stage, F_true) == F_true) return 0;
    if (!data_build->setting.build_sources_library.used && !data_build->setting.build_sources_library_shared.used) return 0;

    if (data->main->output.verbosity != f_console_verbosity_quiet_e && data->main->output.verbosity != f_console_verbosity_error_e) {
      fll_print_format("%r%[Compiling shared library.%]%r", data->main->output.to.stream, f_string_eol_s, data->main->context.set.important, data->main->context.set.important, f_string_eol_s);
    }

    f_string_dynamics_t arguments = f_string_dynamics_t_initialize;

    uint8_t i = 0;

    {
      f_number_unsigned_t j = 0;

      f_string_statics_t * const values[] = {
        &data_build->setting.build_compiler_arguments,
        &data_build->setting.build_compiler_arguments_shared,
        &data_build->setting.build_compiler_arguments_library,
        &data_build->setting.build_compiler_arguments_library_shared,
      };

      for (i = 0; i < 4; ++i) {

        for (j = 0; j < values[i]->used; ++j) {

          if (!values[i]->array[j].used) continue;

          *status = fll_execute_arguments_add(values[i]->array[j], &arguments);
          if (F_status_is_error(*status)) break;
        } // for

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fll_execute_arguments_add", F_true);

          f_string_dynamics_resize(0, &arguments);

          return 0;
        }
      } // for
    }

    *status = fake_build_objects_add(data, data_build, &data->path_build_objects_shared, &data_build->setting.build_objects_library, &data_build->setting.build_objects_library_shared, &arguments);

    if (F_status_is_error(*status)) {
      fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_objects_add", F_true);

      f_string_dynamics_resize(0, &arguments);

      return 0;
    }

    *status = fake_build_sources_add(data, data_build, &data_build->setting.path_sources_library, &data_build->setting.build_sources_library, &data_build->setting.build_sources_library_shared, &arguments);

    if (F_status_is_error(*status)) {
      fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_sources_add", F_true);

      f_string_dynamics_resize(0, &arguments);

      return 0;
    }

    f_string_static_t parameter_file_name = f_string_static_t_initialize;
    f_string_static_t parameter_file_name_major = f_string_static_t_initialize;
    f_string_static_t parameter_file_name_minor = f_string_static_t_initialize;
    f_string_static_t parameter_file_name_micro = f_string_static_t_initialize;
    f_string_static_t parameter_file_name_nano = f_string_static_t_initialize;

    f_array_length_t strings_length = 1;

    parameter_file_name.used = fake_build_parameter_library_name_prefix_s.used + data_build->setting.build_name.used + fake_build_parameter_library_name_suffix_shared_s.used;

    if (data_build->setting.version_major.used) {
      parameter_file_name_major.used = parameter_file_name.used + data_build->setting.version_major_prefix.used + data_build->setting.version_major.used;
      strings_length = 2;

      if (data_build->setting.version_minor.used) {
        parameter_file_name_minor.used = parameter_file_name_major.used + data_build->setting.version_minor_prefix.used + data_build->setting.version_minor.used;
        strings_length = 3;

        if (data_build->setting.version_micro.used) {
          parameter_file_name_micro.used = parameter_file_name_minor.used + data_build->setting.version_micro_prefix.used + data_build->setting.version_micro.used;
          strings_length = 4;

          if (data_build->setting.version_nano.used) {
            parameter_file_name_nano.used =  parameter_file_name_micro.used + data_build->setting.version_nano_prefix.used + data_build->setting.version_nano.used;
            strings_length = 5;
          }
        }
      }
    }

    f_char_t parameter_file_name_string[parameter_file_name.used + 1];
    f_char_t parameter_file_name_major_string[parameter_file_name_major.used + 1];
    f_char_t parameter_file_name_minor_string[parameter_file_name_minor.used + 1];
    f_char_t parameter_file_name_micro_string[parameter_file_name_micro.used + 1];
    f_char_t parameter_file_name_nano_string[parameter_file_name_nano.used + 1];

    parameter_file_name.string = parameter_file_name_string;
    parameter_file_name_major.string = parameter_file_name_major_string;
    parameter_file_name_minor.string = parameter_file_name_minor_string;
    parameter_file_name_micro.string = parameter_file_name_micro_string;
    parameter_file_name_nano.string = parameter_file_name_nano_string;

    parameter_file_name.used = 0;
    parameter_file_name_major.used = 0;
    parameter_file_name_minor.used = 0;
    parameter_file_name_micro.used = 0;
    parameter_file_name_nano.used = 0;

    f_string_static_t *strings[5] = {
      &parameter_file_name,
      &parameter_file_name_major,
      &parameter_file_name_minor,
      &parameter_file_name_micro,
      &parameter_file_name_nano,
    };

    {
      for (i = 0; i < strings_length; ++i) {

        memcpy(strings[i]->string, fake_build_parameter_library_name_prefix_s.string, sizeof(f_char_t) * fake_build_parameter_library_name_prefix_s.used);

        strings[i]->used += fake_build_parameter_library_name_prefix_s.used;
      } // for

      for (i = 0; i < strings_length; ++i) {

        memcpy(strings[i]->string + strings[i]->used, data_build->setting.build_name.string, sizeof(f_char_t) * data_build->setting.build_name.used);

        strings[i]->used += data_build->setting.build_name.used;
      } // for

      for (i = 0; i < strings_length; ++i) {

        memcpy(strings[i]->string + strings[i]->used, fake_build_parameter_library_name_suffix_shared_s.string, sizeof(f_char_t) * fake_build_parameter_library_name_suffix_shared_s.used);

        strings[i]->used += fake_build_parameter_library_name_suffix_shared_s.used;
      } // for

      if (data_build->setting.version_major.used) {
        if (data_build->setting.version_major_prefix.used) {
          for (i = 1; i < strings_length; ++i) {

            memcpy(strings[i]->string + strings[i]->used, data_build->setting.version_major_prefix.string, sizeof(f_char_t) * data_build->setting.version_major_prefix.used);

            strings[i]->used += data_build->setting.version_major_prefix.used;
          } // for
        }

        for (i = 1; i < strings_length; ++i) {

          memcpy(strings[i]->string + strings[i]->used, data_build->setting.version_major.string, sizeof(f_char_t) * data_build->setting.version_major.used);

          strings[i]->used += data_build->setting.version_major.used;
        } // for

        if (data_build->setting.version_minor.used) {
          if (data_build->setting.version_minor_prefix.used) {
            for (i = 2; i < strings_length; ++i) {

              memcpy(strings[i]->string + strings[i]->used, data_build->setting.version_minor_prefix.string, sizeof(f_char_t) * data_build->setting.version_minor_prefix.used);

              strings[i]->used += data_build->setting.version_minor_prefix.used;
            } // for
          }

          for (i = 2; i < strings_length; ++i) {

            memcpy(strings[i]->string + strings[i]->used, data_build->setting.version_minor.string, sizeof(f_char_t) * data_build->setting.version_minor.used);

            strings[i]->used += data_build->setting.version_minor.used;
          } // for

          if (data_build->setting.version_micro.used) {
            if (data_build->setting.version_micro_prefix.used) {
              for (i = 3; i < strings_length; ++i) {

                memcpy(strings[i]->string + strings[i]->used, data_build->setting.version_micro_prefix.string, sizeof(f_char_t) * data_build->setting.version_micro_prefix.used);

                strings[i]->used += data_build->setting.version_micro_prefix.used;
              } // for
            }

            for (i = 3; i < strings_length; ++i) {

              memcpy(strings[i]->string + strings[i]->used, data_build->setting.version_micro.string, sizeof(f_char_t) * data_build->setting.version_micro.used);

              strings[i]->used += data_build->setting.version_micro.used;
            } // for

            if (data_build->setting.version_nano.used) {
              if (data_build->setting.version_nano_prefix.used) {
                for (i = 4; i < strings_length; ++i) {

                  memcpy(strings[i]->string + strings[i]->used, data_build->setting.version_nano_prefix.string, sizeof(f_char_t) * data_build->setting.version_nano_prefix.used);

                  strings[i]->used += data_build->setting.version_nano_prefix.used;
                } // for
              }

              for (i = 4; i < strings_length; ++i) {

                memcpy(strings[i]->string + strings[i]->used, data_build->setting.version_nano.string, sizeof(f_char_t) * data_build->setting.version_nano.used);

                strings[i]->used += data_build->setting.version_nano.used;
              } // for
            }
          }
        }
      }

      parameter_file_name_string[parameter_file_name.used] = 0;
      parameter_file_name_major_string[parameter_file_name_major.used] = 0;
      parameter_file_name_minor_string[parameter_file_name_minor.used] = 0;
      parameter_file_name_micro_string[parameter_file_name_micro.used] = 0;
      parameter_file_name_nano_string[parameter_file_name_nano.used] = 0;

      f_string_static_t parameter_file_path = f_string_static_t_initialize;
      f_string_static_t parameter_linker = f_string_static_t_initialize;

      parameter_file_path.used = data->path_build_libraries_shared.used;
      parameter_linker.used = fake_build_parameter_library_shared_prefix_s.used;

      if (data_build->setting.version_file == fake_build_version_type_major_e) {
        parameter_file_path.used += parameter_file_name_major.used;
      }
      else if (data_build->setting.version_file == fake_build_version_type_minor_e) {
        parameter_file_path.used += parameter_file_name_minor.used;
      }
      else if (data_build->setting.version_file == fake_build_version_type_micro_e) {
        parameter_file_path.used += parameter_file_name_micro.used;
      }
      else if (data_build->setting.version_file == fake_build_version_type_nano_e) {
        parameter_file_path.used += parameter_file_name_nano.used;
      }

      if (data_build->setting.version_target == fake_build_version_type_major_e) {
        parameter_linker.used += parameter_file_name_major.used;
      }
      else if (data_build->setting.version_target == fake_build_version_type_minor_e) {
        parameter_linker.used += parameter_file_name_minor.used;
      }
      else if (data_build->setting.version_target == fake_build_version_type_micro_e) {
        parameter_linker.used += parameter_file_name_micro.used;
      }
      else if (data_build->setting.version_target == fake_build_version_type_nano_e) {
        parameter_linker.used += parameter_file_name_nano.used;
      }

      f_char_t parameter_file_path_string[parameter_file_path.used + 1];
      f_char_t parameter_linker_string[parameter_linker.used + 1];

      parameter_file_path.string = parameter_file_path_string;
      parameter_linker.string = parameter_linker_string;

      parameter_file_path_string[parameter_file_path.used] = 0;
      parameter_linker_string[parameter_linker.used] = 0;

      memcpy(parameter_file_path_string, data->path_build_libraries_shared.string, sizeof(f_char_t) * data->path_build_libraries_shared.used);
      memcpy(parameter_linker_string, fake_build_parameter_library_shared_prefix_s.string, sizeof(f_char_t) * fake_build_parameter_library_shared_prefix_s.used);

      if (data_build->setting.version_file == fake_build_version_type_major_e) {
        memcpy(parameter_file_path_string + data->path_build_libraries_shared.used, parameter_file_name_major_string, sizeof(f_char_t) * parameter_file_name_major.used);
      }
      else if (data_build->setting.version_file == fake_build_version_type_minor_e) {
        memcpy(parameter_file_path_string + data->path_build_libraries_shared.used, parameter_file_name_minor_string, sizeof(f_char_t) * parameter_file_name_minor.used);
      }
      else if (data_build->setting.version_file == fake_build_version_type_micro_e) {
        memcpy(parameter_file_path_string + data->path_build_libraries_shared.used, parameter_file_name_micro_string, sizeof(f_char_t) * parameter_file_name_micro.used);
      }
      else if (data_build->setting.version_file == fake_build_version_type_nano_e) {
        memcpy(parameter_file_path_string + data->path_build_libraries_shared.used, parameter_file_name_nano_string, sizeof(f_char_t) * parameter_file_name_nano.used);
      }

      if (data_build->setting.version_target == fake_build_version_type_major_e) {
        memcpy(parameter_linker_string + fake_build_parameter_library_shared_prefix_s.used, parameter_file_name_major_string, sizeof(f_char_t) * parameter_file_name_major.used);
      }
      else if (data_build->setting.version_target == fake_build_version_type_minor_e) {
        memcpy(parameter_linker_string + fake_build_parameter_library_shared_prefix_s.used, parameter_file_name_minor_string, sizeof(f_char_t) * parameter_file_name_minor.used);
      }
      else if (data_build->setting.version_target == fake_build_version_type_micro_e) {
        memcpy(parameter_linker_string + fake_build_parameter_library_shared_prefix_s.used, parameter_file_name_micro_string, sizeof(f_char_t) * parameter_file_name_micro.used);
      }
      else if (data_build->setting.version_target == fake_build_version_type_nano_e) {
        memcpy(parameter_linker_string + fake_build_parameter_library_shared_prefix_s.used, parameter_file_name_nano_string, sizeof(f_char_t) * parameter_file_name_nano.used);
      }

      const f_string_static_t values[] = {
        fake_build_parameter_library_shared_s,
        parameter_linker,
        fake_build_parameter_library_output_s,
        parameter_file_path,
      };

      for (i = 0; i < 4; ++i) {

        if (!values[i].used) continue;

        *status = fll_execute_arguments_add(values[i], &arguments);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fll_execute_arguments_add", F_true);

          f_string_dynamics_resize(0, &arguments);

          return 0;
        }
      } // for
    }

    fake_build_arguments_standard_add(data, data_build, F_true, fake_build_type_library_e, &arguments, status);

    if (F_status_is_error(*status)) {
      fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_arguments_standard_add", F_true);

      f_string_dynamics_resize(0, &arguments);

      return 0;
    }

    {
      const int result = fake_execute(data, data_build->environment, data_build->setting.build_compiler, arguments, status);

      f_string_dynamics_resize(0, &arguments);

      if (F_status_is_error(*status)) return 0;
      if (*status == F_child) return result;
    }

    {
      if (parameter_file_name_major.used) {
        f_string_static_t parameter_file_path = f_string_static_t_initialize;
        parameter_file_path.used = data->path_build_libraries_shared.used + parameter_file_name.used;

        f_char_t parameter_file_path_string[parameter_file_path.used + 1];
        parameter_file_path.string = parameter_file_path_string;
        parameter_file_path_string[parameter_file_path.used] = 0;

        memcpy(parameter_file_path_string, data->path_build_libraries_shared.string, sizeof(f_char_t) * data->path_build_libraries_shared.used);
        memcpy(parameter_file_path_string + data->path_build_libraries_shared.used, parameter_file_name.string, sizeof(f_char_t) * parameter_file_name.used);

        *status = f_file_link(parameter_file_name_major, parameter_file_path);

        if (F_status_is_error_not(*status) && data->main->error.verbosity >= f_console_verbosity_verbose_e) {
          fll_print_format("Linked file '%Q' to '%Q'.%r", data->main->output.to.stream, parameter_file_path, parameter_file_name_major, f_string_eol_s);
        }
        else if (F_status_is_error(*status)) {
          if (F_status_set_fine(*status) == F_file_found) {
            fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_file_link", F_true, parameter_file_path, f_file_operation_link_s, fll_error_file_type_file_e);

            return 0;
          }

          fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_file_link", F_true, parameter_file_name_major, f_file_operation_link_s, fll_error_file_type_file_e);

          return 0;
        }
      }

      if (data_build->setting.version_file != fake_build_version_type_major_e && parameter_file_name_major.used) {
        f_string_static_t parameter_file_path = f_string_static_t_initialize;

        {
          parameter_file_path.used = data->path_build_libraries_shared.used + parameter_file_name_major.used;

          f_char_t parameter_file_path_string[parameter_file_path.used + 1];
          parameter_file_path.string = parameter_file_path_string;
          parameter_file_path_string[parameter_file_path.used] = 0;

          memcpy(parameter_file_path_string, data->path_build_libraries_shared.string, sizeof(f_char_t) * data->path_build_libraries_shared.used);
          memcpy(parameter_file_path_string + data->path_build_libraries_shared.used, parameter_file_name_major.string, sizeof(f_char_t) * parameter_file_name_major.used);

          *status = f_file_link(parameter_file_name_minor, parameter_file_path);

          if (F_status_is_error_not(*status) && data->main->error.verbosity >= f_console_verbosity_verbose_e) {
            fll_print_format("Linked file '%Q' to '%Q'.%r", data->main->output.to.stream, parameter_file_path, parameter_file_name_minor, f_string_eol_s);
          }
          else if (F_status_is_error(*status)) {
            if (F_status_set_fine(*status) == F_file_found) {
              fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_file_link", F_true, parameter_file_path, f_file_operation_link_s, fll_error_file_type_file_e);

              return 0;
            }

            fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_file_link", F_true, parameter_file_name_minor, f_file_operation_link_s, fll_error_file_type_file_e);

            return 0;
          }
        }

        if (data_build->setting.version_file != fake_build_version_type_minor_e && parameter_file_name_minor.used) {
          {
            parameter_file_path.used = data->path_build_libraries_shared.used + parameter_file_name_minor.used;

            f_char_t parameter_file_path_string[parameter_file_path.used + 1];
            parameter_file_path.string = parameter_file_path_string;
            parameter_file_path_string[parameter_file_path.used] = 0;

            memcpy(parameter_file_path_string, data->path_build_libraries_shared.string, sizeof(f_char_t) * data->path_build_libraries_shared.used);
            memcpy(parameter_file_path_string + data->path_build_libraries_shared.used, parameter_file_name_minor.string, sizeof(f_char_t) * parameter_file_name_minor.used);

            *status = f_file_link(parameter_file_name_micro, parameter_file_path);

            if (F_status_is_error_not(*status) && data->main->error.verbosity >= f_console_verbosity_verbose_e) {
              fll_print_format("Linked file '%Q' to '%Q'.%r", data->main->output.to.stream, parameter_file_path, parameter_file_name_micro, f_string_eol_s);
            }
            else if (F_status_is_error(*status)) {
              if (F_status_set_fine(*status) == F_file_found) {
                fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_file_link", F_true, parameter_file_path, f_file_operation_link_s, fll_error_file_type_file_e);

                return 0;
              }

              fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_file_link", F_true, parameter_file_name_micro, f_file_operation_link_s, fll_error_file_type_file_e);

              return 0;
            }
          }

          if (data_build->setting.version_file != fake_build_version_type_micro_e && parameter_file_name_micro.used) {
            parameter_file_path.used = data->path_build_libraries_shared.used + parameter_file_name_micro.used;

            f_char_t parameter_file_path_string[parameter_file_path.used + 1];
            parameter_file_path.string = parameter_file_path_string;
            parameter_file_path_string[parameter_file_path.used] = 0;

            memcpy(parameter_file_path_string, data->path_build_libraries_shared.string, sizeof(f_char_t) * data->path_build_libraries_shared.used);
            memcpy(parameter_file_path_string + data->path_build_libraries_shared.used, parameter_file_name_micro.string, sizeof(f_char_t) * parameter_file_name_micro.used);

            *status = f_file_link(parameter_file_name_nano, parameter_file_path);

            if (F_status_is_error_not(*status) && data->main->error.verbosity >= f_console_verbosity_verbose_e) {
              fll_print_format("Linked file '%Q' to '%Q'.%r", data->main->output.to.stream, parameter_file_path, parameter_file_name_nano, f_string_eol_s);
            }
            else if (F_status_is_error(*status)) {
              if (F_status_set_fine(*status) == F_file_found) {
                fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_file_link", F_true, parameter_file_path, f_file_operation_link_s, fll_error_file_type_file_e);

                return 0;
              }

              fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_file_link", F_true, parameter_file_name_nano, f_file_operation_link_s, fll_error_file_type_file_e);

              return 0;
            }
          }
        }
      }
    }

    fake_build_touch(data, file_stage, status);

    return 0;
  }
#endif // _di_fake_build_library_shared_

#ifndef _di_fake_build_library_static_
  int fake_build_library_static(fake_data_t * const data, fake_build_data_t * const data_build, const f_mode_t mode, const f_string_static_t file_stage, f_status_t * const status) {

    if (*status == F_child) return data->main->child;
    if (F_status_is_error(*status) || f_file_exists(file_stage, F_true) == F_true) return 0;
    if (!data_build->setting.build_sources_library.used && !data_build->setting.build_sources_library_static.used) return 0;

    if (data->main->output.verbosity != f_console_verbosity_quiet_e && data->main->output.verbosity != f_console_verbosity_error_e) {
      fll_print_format("%r%[Compiling static library.%]%r", data->main->output.to.stream, f_string_eol_s, data->main->context.set.important, data->main->context.set.important, f_string_eol_s);
    }

    f_string_dynamic_t file_name = f_string_dynamic_t_initialize;
    f_string_dynamic_t source_path = f_string_dynamic_t_initialize;
    f_string_dynamics_t arguments = f_string_dynamics_t_initialize;

    f_array_length_t i = 0;
    f_array_length_t j = 0;

    {
      f_string_statics_t * const values[] = {
        &data_build->setting.build_compiler_arguments,
        &data_build->setting.build_compiler_arguments_static,
        &data_build->setting.build_compiler_arguments_library,
        &data_build->setting.build_compiler_arguments_library_static,
      };

      for (i = 0; i < 4; ++i) {

        for (j = 0; j < values[i]->used; ++j) {

          if (!values[i]->array[j].used) continue;

          *status = fll_execute_arguments_add(values[i]->array[j], &arguments);
          if (F_status_is_error(*status)) break;
        } // for

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fll_execute_arguments_add", F_true);

          f_string_dynamics_resize(0, &arguments);

          return 0;
        }
      } // for
    }

    for (i = 0; i < data_build->setting.build_indexer_arguments.used; ++i) {

      if (!data_build->setting.build_indexer_arguments.array[i].used) continue;

      *status = fll_execute_arguments_add(data_build->setting.build_indexer_arguments.array[i], &arguments);
      if (F_status_is_error(*status)) break;
    } // for

    if (F_status_is_error_not(*status)) {
      f_string_static_t destination = f_string_static_t_initialize;
      destination.used = data->path_build_libraries_static.used + fake_build_parameter_library_name_prefix_s.used;
      destination.used += data_build->setting.build_name.used + fake_build_parameter_library_name_suffix_static_s.used;

      f_char_t destination_string[destination.used + 1];
      destination.string = destination_string;
      destination_string[destination.used] = 0;
      destination.used = 0;

      memcpy(destination_string, data->path_build_libraries_static.string, sizeof(f_char_t) * data->path_build_libraries_static.used);
      destination.used += data->path_build_libraries_static.used;

      memcpy(destination_string + destination.used, fake_build_parameter_library_name_prefix_s.string, sizeof(f_char_t) * fake_build_parameter_library_name_prefix_s.used);
      destination.used += fake_build_parameter_library_name_prefix_s.used;

      memcpy(destination_string + destination.used, data_build->setting.build_name.string, sizeof(f_char_t) * data_build->setting.build_name.used);
      destination.used += data_build->setting.build_name.used;

      memcpy(destination_string + destination.used, fake_build_parameter_library_name_suffix_static_s.string, sizeof(f_char_t) * fake_build_parameter_library_name_suffix_static_s.used);
      destination.used += fake_build_parameter_library_name_suffix_static_s.used;

      *status = fll_execute_arguments_add(destination, &arguments);
    }

    if (F_status_is_error_not(*status)) {
      *status = fake_build_objects_add(data, data_build, &data->path_build_objects_static, &data_build->setting.build_objects_library, &data_build->setting.build_objects_library_static, &arguments);

      if (F_status_is_error(*status)) {
        fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_objects_add", F_true);
      }
    }

    if (F_status_is_error_not(*status)) {
      f_string_static_t source = f_string_static_t_initialize;

      const f_string_dynamics_t *sources[2] = {
        &data_build->setting.build_sources_library,
        &data_build->setting.build_sources_library_static,
      };

      for (i = 0; i < 2; ++i) {

        for (j = 0; j < sources[i]->used; ++j) {

          source_path.used = 0;
          file_name.used = 0;

          *status = fake_build_get_file_name_without_extension(data, sources[i]->array[j], &file_name);

          if (F_status_is_error(*status)) {
            fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_get_file_name_without_extension", F_true);

            break;
          }

          *status = f_file_name_directory(sources[i]->array[j], &source_path);

          if (F_status_is_error(*status)) {
            fll_error_print(data->main->error, F_status_set_fine(*status), "f_file_name_directory", F_true);

            break;
          }

          if (source_path.used) {
            *status = f_string_dynamic_prepend(data->path_build_objects_static, &source_path);

            if (F_status_is_error(*status)) {
              fll_error_print(data->main->error, F_status_set_fine(*status), "f_string_dynamic_prepend", F_true);

              break;
            }

            *status = f_string_dynamic_append_assure(f_path_separator_s, &source_path);

            if (F_status_is_error(*status)) {
              fll_error_print(data->main->error, F_status_set_fine(*status), "f_string_dynamic_append_assure", F_true);

              break;
            }

            source.used = source_path.used + file_name.used + fake_build_parameter_object_name_suffix_s.used;
          }
          else {
            source.used = data->path_build_objects_static.used + file_name.used + fake_build_parameter_object_name_suffix_s.used;
          }

          f_char_t source_string[source.used + 1];
          source.string = source_string;
          source_string[source.used] = 0;

          if (source_path.used) {
            memcpy(source_string, source_path.string, sizeof(f_char_t) * source_path.used);
            memcpy(source_string + source_path.used, file_name.string, sizeof(f_char_t) * file_name.used);
            memcpy(source_string + source_path.used + file_name.used, fake_build_parameter_object_name_suffix_s.string, sizeof(f_char_t) * fake_build_parameter_object_name_suffix_s.used);
          }
          else {
            memcpy(source_string, data->path_build_objects_static.string, sizeof(f_char_t) * data->path_build_objects_static.used);
            memcpy(source_string + data->path_build_objects_static.used, file_name.string, sizeof(f_char_t) * file_name.used);
            memcpy(source_string + data->path_build_objects_static.used + file_name.used, fake_build_parameter_object_name_suffix_s.string, sizeof(f_char_t) * fake_build_parameter_object_name_suffix_s.used);
          }

          *status = fll_execute_arguments_add(source, &arguments);

          if (F_status_is_error(*status)) {
            fll_error_print(data->main->error, F_status_set_fine(*status), "fll_execute_arguments_add", F_true);

            break;
          }
        } // for
      } // for
    }

    int result = data->main->child;

    if (F_status_is_error_not(*status)) {
      result = fake_execute(data, data_build->environment, data_build->setting.build_indexer, arguments, status);
    }

    f_string_dynamic_resize(0, &file_name);
    f_string_dynamic_resize(0, &source_path);
    f_string_dynamics_resize(0, &arguments);

    if (F_status_is_error_not(*status) && *status != F_child) {
      fake_build_touch(data, file_stage, status);
    }

    return result;
  }
#endif // _di_fake_build_library_static_

#ifndef _di_fake_build_library_static_object_
  int fake_build_library_static_object(fake_data_t * const data, fake_build_data_t * const data_build, const f_mode_t mode, const f_string_static_t file_stage, f_status_t * const status) {

    if (*status == F_child) return data->main->child;
    if (F_status_is_error(*status) || f_file_exists(file_stage, F_true) == F_true) return 0;
    if (!data_build->setting.build_sources_library.used && !data_build->setting.build_sources_library_static.used) return 0;

    if (data->main->output.verbosity != f_console_verbosity_quiet_e && data->main->output.verbosity != f_console_verbosity_error_e) {
      fll_print_format("%r%[Compiling static library objects.%]%r", data->main->output.to.stream, f_string_eol_s, data->main->context.set.important, data->main->context.set.important, f_string_eol_s);
    }

    f_string_dynamics_t arguments = f_string_dynamics_t_initialize;
    f_string_dynamic_t file_name = f_string_dynamic_t_initialize;
    f_string_dynamic_t destination_path = f_string_dynamic_t_initialize;
    f_string_static_t destination = f_string_static_t_initialize;
    f_string_static_t source = f_string_static_t_initialize;

    int result = data->main->child;

    const f_string_dynamics_t *sources[2] = {
      &data_build->setting.build_sources_library,
      &data_build->setting.build_sources_library_static,
    };

    f_array_length_t i = 0;
    f_array_length_t j = 0;
    f_array_length_t k = 0;
    uint8_t v = 0;

    for (i = 0; i < 2; ++i) {

      for (j = 0; j < sources[i]->used; ++j) {

        if (!sources[i]->array[j].used) continue;

        file_name.used = 0;
        destination_path.used = 0;

        while (arguments.used) {
          arguments.array[--arguments.used].used = 0;
        } // while

        {
          f_string_statics_t * const values[] = {
            &data_build->setting.build_compiler_arguments,
            &data_build->setting.build_compiler_arguments_static,
            &data_build->setting.build_compiler_arguments_object,
            &data_build->setting.build_compiler_arguments_object_static,
          };

          for (v = 0; v < 4; ++v) {

            for (k = 0; k < values[v]->used; ++k) {

              if (!values[v]->array[k].used) continue;

              *status = fll_execute_arguments_add(values[v]->array[k], &arguments);
              if (F_status_is_error(*status)) break;
            } // for

            if (F_status_is_error(*status)) {
              fll_error_print(data->main->error, F_status_set_fine(*status), "fll_execute_arguments_add", F_true);

              break;
            }
          } // for
        }

        fake_build_path_source_length(data, data_build, &data_build->setting.path_sources_library, &source);

        f_char_t source_string[source.used + sources[i]->array[j].used + 1];
        source.string = source_string;

        fake_build_path_source_string(data, data_build, &data_build->setting.path_sources_library, &source);

        memcpy(source_string + source.used, sources[i]->array[j].string, sizeof(f_char_t) * sources[i]->array[j].used);
        source.used += sources[i]->array[j].used;
        source.string[source.used] = 0;

        *status = fake_build_get_file_name_without_extension(data, sources[i]->array[j], &file_name);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_get_file_name_without_extension", F_true);

          break;
        }

        if (data_build->setting.language == fake_build_language_type_c_e || data_build->setting.language == fake_build_language_type_cpp_e) {
          *status = f_file_name_directory(sources[i]->array[j], &destination_path);

          if (F_status_is_error(*status)) {
            fll_error_print(data->main->error, F_status_set_fine(*status), "f_file_name_directory", F_true);

            break;
          }

          if (destination_path.used) {
            *status = f_string_dynamic_prepend(data->path_build_objects_static, &destination_path);

            if (F_status_is_error(*status)) {
              fll_error_print(data->main->error, F_status_set_fine(*status), "f_string_dynamic_prepend", F_true);

              break;
            }

            *status = f_string_dynamic_append_assure(f_path_separator_s, &destination_path);

            if (F_status_is_error(*status)) {
              fll_error_print(data->main->error, F_status_set_fine(*status), "f_string_dynamic_append_assure", F_true);

              break;
            }

            *status = f_directory_exists(destination_path);

            if (*status == F_false) {
              if (data->main->error.verbosity != f_console_verbosity_quiet_e) {
                flockfile(data->main->error.to.stream);

                fl_print_format("%r%[%QThe path '%]", data->main->error.to.stream, f_string_eol_s, data->main->error.context, data->main->error.prefix, data->main->error.context);
                fl_print_format("%[%Q%]", data->main->error.to.stream, data->main->error.notable, destination_path, data->main->error.notable);
                fl_print_format("%[' exists but is not a directory.%]%r", data->main->error.to.stream, data->main->error.context, data->main->error.context, f_string_eol_s);

                funlockfile(data->main->error.to.stream);
              }

              *status = F_status_set_error(F_failure);

              break;
            }

            if (*status == F_file_found_not) {
              *status = f_directory_create(destination_path, mode.directory);

              if (F_status_is_error(*status)) {
                if (F_status_set_fine(*status) == F_file_found_not) {
                  flockfile(data->main->error.to.stream);

                  fl_print_format("%r%[%QThe path '%]", data->main->error.to.stream, f_string_eol_s, data->main->error.context, data->main->error.prefix, data->main->error.context);
                  fl_print_format("%[%Q%]", data->main->error.to.stream, data->main->error.notable, destination_path, data->main->error.notable);
                  fl_print_format("%[' could not be created, a parent directory does not exist.%]%r", data->main->error.to.stream, data->main->error.context, data->main->error.context, f_string_eol_s);

                  funlockfile(data->main->error.to.stream);
                }
                else {
                  fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_directory_create", F_true, destination_path, f_file_operation_create_s, fll_error_file_type_directory_e);
                }

                break;
              }

              if (data->main->error.verbosity >= f_console_verbosity_verbose_e) {
                fll_print_format("Directory '%Q' created.%r", data->main->output.to.stream, destination_path, f_string_eol_s);
              }
            }

            if (F_status_is_error(*status)) {
              fll_error_file_print(data->main->error, F_status_set_fine(*status), "f_directory_exists", F_true, destination_path, f_file_operation_create_s, fll_error_file_type_directory_e);

              break;
            }

            destination.used = destination_path.used + file_name.used + fake_build_parameter_object_name_suffix_s.used;
          }
          else {
            destination.used = data->path_build_objects_static.used + file_name.used + fake_build_parameter_object_name_suffix_s.used;
          }

          f_char_t destination_string[destination.used + 1];
          destination.string = destination_string;
          destination_string[destination.used] = 0;

          if (destination_path.used) {
            memcpy(destination_string, destination_path.string, sizeof(f_char_t) * destination_path.used);
            memcpy(destination_string + destination_path.used, file_name.string, sizeof(f_char_t) * file_name.used);
            memcpy(destination_string + destination_path.used + file_name.used, fake_build_parameter_object_name_suffix_s.string, sizeof(f_char_t) * fake_build_parameter_object_name_suffix_s.used);
          }
          else {
            memcpy(destination_string, data->path_build_objects_static.string, sizeof(f_char_t) * data->path_build_objects_static.used);
            memcpy(destination_string + data->path_build_objects_static.used, file_name.string, sizeof(f_char_t) * file_name.used);
            memcpy(destination_string + data->path_build_objects_static.used + file_name.used, fake_build_parameter_object_name_suffix_s.string, sizeof(f_char_t) * fake_build_parameter_object_name_suffix_s.used);
          }

          const f_string_static_t values[] = {
            source,
            fake_build_parameter_object_compile_s,
            fake_build_parameter_object_static_s,
            fake_build_parameter_object_output_s,
            destination,
          };

          for (v = 0; v < 5; ++v) {

            if (!values[v].used) continue;

            *status = fll_execute_arguments_add(values[v], &arguments);
            if (F_status_is_error(*status)) break;
          } // for
        }
        else if (source.used) {
          *status = fll_execute_arguments_add(source, &arguments);
          if (F_status_is_error(*status)) break;
        }

        fake_build_arguments_standard_add(data, data_build, F_false, fake_build_type_object_e, &arguments, status);

        if (F_status_is_error(*status)) {
          fll_error_print(data->main->error, F_status_set_fine(*status), "fake_build_arguments_standard_add", F_true);

          break;
        }

        result = fake_execute(data, data_build->environment, data_build->setting.build_compiler, arguments, status);

        macro_f_string_dynamics_t_delete_simple(arguments);

        if (F_status_is_error(*status) || *status == F_child) break;
      } // for

      if (F_status_is_error(*status) || *status == F_child) break;
    } // for

    f_string_dynamic_resize(0, &file_name);
    f_string_dynamic_resize(0, &destination_path);
    f_string_dynamics_resize(0, &arguments);

    if (F_status_is_error_not(*status) && *status != F_child) {
      fake_build_touch(data, file_stage, status);
    }

    return result;
  }
#endif // _di_fake_build_library_static_object_

#ifdef __cplusplus
} // extern "C"
#endif
