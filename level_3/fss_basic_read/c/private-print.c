#include "fss_basic_read.h"
#include "private-common.h"
#include "private-print.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_fss_basic_read_print_at_
  void fss_basic_read_print_at(fll_program_data_t * const main, const f_array_length_t at, const f_fss_delimits_t delimits_object, const f_fss_delimits_t delimits_content, fss_basic_read_data_t * const data) {

    if (at >= data->contents.used) return;

    if (!(data->option & fss_basic_read_data_option_object_d)) {
      if (!(data->option & fss_basic_read_data_option_content_d) || (!data->contents.array[at].used && !(data->option & fss_basic_read_data_option_empty_d))) {
        return;
      }
    }

    // 0x1 = object printed, 0x2 = content printed.
    uint8_t print_state = 0x0;

    flockfile(main->output.to.stream);

    if (data->option & fss_basic_read_data_option_object_d) {
      print_state = 0x1;

      if (data->option & fss_basic_read_data_option_original_d) {
        fss_basic_read_print_quote(main, data, data->quotes.array[at]);

        if (data->option & fss_basic_read_data_option_trim_d) {
          fl_print_trim_dynamic_partial(data->buffer, data->objects.array[at], main->output.to.stream);
        }
        else {
          f_print_dynamic_partial(data->buffer, data->objects.array[at], main->output.to.stream);
        }

        fss_basic_read_print_quote(main, data, data->quotes.array[at]);
      }
      else {
        if (data->option & fss_basic_read_data_option_trim_d) {
          fl_print_trim_except_dynamic_partial(data->buffer, data->objects.array[at], delimits_object, main->output.to.stream);
        }
        else {
          f_print_except_dynamic_partial(data->buffer, data->objects.array[at], delimits_object, main->output.to.stream);
        }
      }
    }

    if ((data->option & fss_basic_read_data_option_content_d)) {
      if (data->contents.array[at].used) {
        if (data->contents.array[at].array[0].start <= data->contents.array[at].array[0].stop) {
          print_state |= 0x2;

          if (data->option & fss_basic_read_data_option_object_d) {
            fss_basic_read_print_object_end(main);
          }
        }

        if (data->option & fss_basic_read_data_option_original_d) {
          f_print_dynamic_partial(data->buffer, data->contents.array[at].array[0], main->output.to.stream);
        }
        else {
          f_print_except_dynamic_partial(data->buffer, data->contents.array[at].array[0], delimits_content, main->output.to.stream);
        }
      }
    }

    if (print_state & 0x3) {
      fss_basic_read_print_set_end(main);
    }
    else if (data->option & fss_basic_read_data_option_content_d) {
      fss_basic_read_print_content_end_empty(main);
    }

    funlockfile(main->output.to.stream);
  }
#endif // _di_fss_basic_read_print_at_

#ifndef _di_fss_basic_read_print_content_end_empty_
  void fss_basic_read_print_content_end_empty(fll_program_data_t * const main) {

    if (main->parameters.array[fss_basic_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_basic_read_pipe_content_end_s, main->output.to.stream);
    }
  }
#endif // _di_fss_basic_read_print_content_end_empty_

#ifndef _di_fss_basic_read_print_object_end_
  void fss_basic_read_print_object_end(fll_program_data_t * const main) {

    if (main->parameters.array[fss_basic_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_basic_read_pipe_content_start_s, main->output.to.stream);
    }
    else {
      f_print_dynamic_raw(f_fss_space_s, main->output.to.stream);
    }
  }
#endif // _di_fss_basic_read_print_object_end_

#ifndef _di_fss_basic_read_print_one_
  void fss_basic_read_print_one(fll_program_data_t * const main) {

    f_print_dynamic_raw(f_string_ascii_1_s, main->output.to.stream);
    f_print_dynamic_raw(f_string_eol_s, main->output.to.stream);
  }
#endif // _di_fss_basic_read_print_one_

#ifndef _di_fss_basic_read_print_quote_
  uint8_t fss_basic_read_print_quote(fll_program_data_t * const main, fss_basic_read_data_t * const data, const f_fss_quote_t quote) {

    if (!(data->option & fss_basic_read_data_option_original_d) || !quote) return F_false;

    f_print_dynamic_raw(
      quote == f_fss_quote_type_single_e
        ? f_fss_quote_single_s
        : quote == f_fss_quote_type_double_e
          ? f_fss_quote_double_s
          : f_fss_quote_backtick_s,
      main->output.to.stream
    );

    return F_true;
  }
#endif // _di_fss_basic_read_print_quote_

#ifndef _di_fss_basic_read_print_set_end_
  void fss_basic_read_print_set_end(fll_program_data_t * const main) {

    if (main->parameters.array[fss_basic_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_basic_read_pipe_content_end_s, main->output.to.stream);
    }
    else {
      f_print_dynamic_raw(f_string_eol_s, main->output.to.stream);
    }
  }
#endif // _di_fss_basic_read_print_set_end_

#ifndef _di_fss_basic_read_print_zero_
  void fss_basic_read_print_zero(fll_program_data_t * const main) {

    f_print_dynamic_raw(f_string_ascii_0_s, main->output.to.stream);
    f_print_dynamic_raw(f_string_eol_s, main->output.to.stream);
  }
#endif // _di_fss_basic_read_print_zero_

#ifdef __cplusplus
} // extern "C"
#endif
