#include "fss_embedded_list_read.h"
#include "private-common.h"
#include "private-print.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _di_fss_embedded_list_read_print_at_determine_begin_
  f_string_range_t fss_embedded_list_read_print_at_determine_begin(fss_embedded_list_read_data_t * const data, const f_string_range_t object, const f_string_ranges_t contents) {

    f_string_range_t before = f_string_range_t_initialize;

    if (contents.used && contents.array[0].start <= contents.array[0].stop) {
      if (contents.array[0].stop + 1 < data->buffer.used) {
        before.start = contents.array[0].stop + 1;

        // Seek past any NULLs.
        while (before.start < data->buffer.used && !data->buffer.string[before.start]) ++before.start;
      }
      else {
        return before;
      }
    }
    else if (object.start > object.stop) {
      return before;
    }
    else {
      before.start = object.stop;

      while (before.start < data->buffer.used && data->buffer.string[before.start] != f_fss_eol_s.string[0]) ++before.start;

      if (before.start >= data->buffer.used || data->buffer.string[before.start] != f_fss_eol_s.string[0]) {
        before.start = 1;
        before.stop = 0;

        return before;
      }

      ++before.start;
    }

    if (before.start < data->buffer.used) {
      before.stop = before.start;

      while (before.stop < data->buffer.used && data->buffer.string[before.stop] != f_fss_embedded_list_close_s.string[0]) ++before.stop;

      if (before.stop < data->buffer.used && data->buffer.string[before.stop] == f_fss_embedded_list_close_s.string[0]) {
        --before.stop;
      }
      else {
        before.start = 1;
        before.stop = 0;
      }
    }
    else {
      before.start = 1;
      before.stop = 0;
    }

    return before;
  }
#endif // _di_fss_embedded_list_read_print_at_determine_begin_

#ifndef _di_fss_embedded_list_read_print_content_ignore_
  void fss_embedded_list_read_print_content_ignore(fss_embedded_list_read_data_t * const data) {

    if (data->main->parameters.array[fss_embedded_list_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_embedded_list_read_pipe_content_ignore_s, data->main->output.to.stream);
    }
  }
#endif // _di_fss_embedded_list_read_print_content_ignore_

#ifndef _di_fss_embedded_list_read_print_object_
  void fss_embedded_list_read_print_object(fss_embedded_list_read_data_t * const data, const f_string_range_t object, const f_string_ranges_t contents, const f_array_lengths_t objects_delimits, f_status_t (*print_object)(const f_string_static_t, const f_string_range_t, const f_array_lengths_t, FILE *)) {

    if (!data->buffer.used) return;

    if (data->main->parameters.array[fss_embedded_list_read_parameter_content_e].result == f_console_result_found_e && data->main->parameters.array[fss_embedded_list_read_parameter_original_e].result == f_console_result_none_e) {
      const f_string_range_t before = fss_embedded_list_read_print_at_determine_begin(data, object, contents);

      if (before.start <= before.stop) {
        f_print_except_dynamic_partial(data->buffer, before, objects_delimits, data->main->output.to.stream);
      }
    }

    f_string_range_t before = f_string_range_t_initialize;
    f_string_range_t after = f_string_range_t_initialize;

    if (data->main->parameters.array[fss_embedded_list_read_parameter_original_e].result == f_console_result_found_e) {
      if (object.start <= object.stop) {
        if (object.start) {
          before.start = before.stop = object.start - 1;

          while (before.start && data->buffer.string[before.start] != f_fss_eol_s.string[0]) --before.start;

          if (data->buffer.string[before.start] == f_fss_eol_s.string[0]) ++before.start;
        }

        if (data->main->parameters.array[fss_embedded_list_read_parameter_trim_e].result == f_console_result_none_e) {
          if (f_fss_embedded_list_open_s.used && object.stop + 1 < data->buffer.used) {
            after.start = after.stop = object.stop + 1;

            if (data->buffer.string[after.stop] == f_fss_embedded_list_open_s.string[0]) {
              after.start = 1;
              after.stop = 0;
            }
            else {
              while (after.stop < data->buffer.used && data->buffer.string[after.stop] != f_fss_embedded_list_open_s.string[0]) ++after.stop;

              if (after.stop == data->buffer.used || data->buffer.string[after.stop] == f_fss_embedded_list_open_s.string[0]) --after.stop;
            }
          }
        }
      }

      // Handle case where empty Object is empty because it has spaces and line it up with the close.
      else if (data->main->parameters.array[fss_embedded_list_read_parameter_content_e].result == f_console_result_found_e) {
        const f_string_range_t before = fss_embedded_list_read_print_at_determine_begin(data, object, contents);

        if (before.start <= before.stop) {
          f_print_except_dynamic_partial(data->buffer, before, objects_delimits, data->main->output.to.stream);
        }
      }
    }

    if (before.start <= before.stop) {
      f_print_except_dynamic_partial(data->buffer, before, objects_delimits, data->main->output.to.stream);
    }

    print_object(data->buffer, object, objects_delimits, data->main->output.to.stream);

    if (after.start <= after.stop) {
      f_print_except_dynamic_partial(data->buffer, after, objects_delimits, data->main->output.to.stream);
    }
  }
#endif // _di_fss_embedded_list_read_print_object_

#ifndef _di_fss_embedded_list_read_print_object_end_
  void fss_embedded_list_read_print_object_end(fss_embedded_list_read_data_t * const data) {

    if (data->main->parameters.array[fss_embedded_list_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_embedded_list_read_pipe_content_start_s, data->main->output.to.stream);
    }
    else {
      if (data->main->parameters.array[fss_embedded_list_read_parameter_object_e].result == f_console_result_found_e && data->main->parameters.array[fss_embedded_list_read_parameter_content_e].result == f_console_result_found_e) {
        f_print_dynamic_raw(f_fss_embedded_list_open_s, data->main->output.to.stream);
        f_print_dynamic_raw(f_fss_embedded_list_open_end_s, data->main->output.to.stream);
      }
      else {
        f_print_dynamic_raw(f_fss_eol_s, data->main->output.to.stream);
      }
    }
  }
#endif // _di_fss_embedded_list_read_print_object_end_

#ifndef _di_fss_embedded_list_read_print_set_end_
  void fss_embedded_list_read_print_set_end(fss_embedded_list_read_data_t * const data, f_fss_items_t * const items, const f_array_length_t at, const f_fss_delimits_t objects_delimits) {

    if (data->main->parameters.array[fss_embedded_list_read_parameter_pipe_e].result == f_console_result_found_e) {
      f_print_dynamic_raw(fss_embedded_list_read_pipe_content_end_s, data->main->output.to.stream);
    }
    else {
      if (data->main->parameters.array[fss_embedded_list_read_parameter_object_e].result == f_console_result_found_e && data->main->parameters.array[fss_embedded_list_read_parameter_content_e].result == f_console_result_found_e) {
        const f_string_range_t before = fss_embedded_list_read_print_at_determine_begin(data, items->array[at].object, items->array[at].content);

        if (before.start <= before.stop) {
          f_print_except_dynamic_partial(data->buffer, before, objects_delimits, data->main->output.to.stream);
        }

        f_print_dynamic_raw(f_fss_embedded_list_close_s, data->main->output.to.stream);
        f_print_dynamic_raw(f_fss_embedded_list_close_end_s, data->main->output.to.stream);
      }
      else {
        f_print_dynamic_raw(f_fss_eol_s, data->main->output.to.stream);
      }
    }
  }
#endif // _di_fss_embedded_list_read_print_set_end_

#ifdef __cplusplus
} // extern "C"
#endif
