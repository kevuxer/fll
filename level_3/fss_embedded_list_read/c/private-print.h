/**
 * FLL - Level 3
 *
 * Project: FSS
 * API Version: 0.6
 * Licenses: lgpl-2.1-or-later
 */
#ifndef _PRIVATE_print_h
#define _PRIVATE_print_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Determine the spacing before the Content close.
 *
 * This is intended to be used to determine the spacing before both an Object and a Content when using the appropriate modes.
 *
 * @param data
 *   The program data.
 * @param object
 *   The range within data.buffer representing the Object.
 * @param contents
 *   The ranges within data.buffer representing the Content.
 *
 * @return
 *   The range found.
 *   The range.start = 1 and range.stop = 0 is returned if not found.
 */
#ifndef _di_fss_embedded_list_read_print_at_determine_begin_
  extern f_string_range_t fss_embedded_list_read_print_at_determine_begin(fss_embedded_list_read_data_t * const data, const f_string_range_t object, const f_string_ranges_t contents) F_attribute_visibility_internal_d;
#endif // _di_fss_embedded_list_read_print_at_determine_begin_

/**
 * Print the ignore character for content.
 *
 * This is only used in pipe output mode.
 *
 * @param data
 *   The program data.
 */
#ifndef _di_fss_embedded_list_read_print_content_ignore_
  extern void fss_embedded_list_read_print_content_ignore(fss_embedded_list_read_data_t * const data) F_attribute_visibility_internal_d;
#endif // _di_fss_embedded_list_read_print_content_ignore_

/**
 * Print the Object.
 *
 * @param data
 *   The program data.
 * @param object
 *   The range within data.buffer representing the Object.
 * @param contents
 *   The ranges within data.buffer representing the Content.
 *   This is used for determining the begin locations.
 * @param objects_delimits
 *   The Object delimits array.
 * @param print_object
 *   A callback for performing the printing of the Object.
 */
#ifndef _di_fss_embedded_list_read_print_object_
  extern void fss_embedded_list_read_print_object(fss_embedded_list_read_data_t * const data, const f_string_range_t object, const f_string_ranges_t contents, const f_array_lengths_t objects_delimits, f_status_t (*print_object)(const f_string_static_t, const f_string_range_t, const f_array_lengths_t, FILE *)) F_attribute_visibility_internal_d;
#endif // _di_fss_embedded_list_read_print_object_

/**
 * Print the end of an object (which is essentially the start of a content).
 *
 * @param data
 *   The program data.
 */
#ifndef _di_fss_embedded_list_read_print_object_end_
  extern void fss_embedded_list_read_print_object_end(fss_embedded_list_read_data_t * const data) F_attribute_visibility_internal_d;
#endif // _di_fss_embedded_list_read_print_object_end_

/**
 * Print the end of an object/content set.
 *
 * @param data
 *   The program data.
 * @param items
 *   The items set to process.
 * @param at
 *   The position within the Object and Contents array to print.
 * @param objects_delimits
 *   The Object delimits array.
 */
#ifndef _di_fss_embedded_list_read_print_set_end_
  extern void fss_embedded_list_read_print_set_end(fss_embedded_list_read_data_t * const data, f_fss_items_t * const items, const f_array_length_t at, const f_fss_delimits_t objects_delimits) F_attribute_visibility_internal_d;
#endif // _di_fss_embedded_list_read_print_set_end_

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _PRIVATE_print_h
