#!/bin/bash
# license: lgpl-2.1-or-later
# programmer: Kevin Day
#
# A simple script for generating ctags for use by editors, such as geany.
#
# This is intended to be run against a compile package path.
# The default source code structure in this repository is not ideal for generating ctags.
# Instead, build the library, such as is done by the bootstrap-example.sh script.
#

main() {

  if [[ ${SHELL_ENGINE} == "zsh" ]] ; then
    emulate ksh
  fi

  local public_name="Simple FLL Generate CTags Script"
  local system_name=generate_ctags
  local called_name=$(basename ${0})
  local version=0.6.12

  local grab_next=
  local do_color=dark
  local do_help=
  local do_copyright=
  local i=0
  local p=
  local t=0

  local c_reset="\\033[0m"
  local c_title="\\033[1;33m"
  local c_error="\\033[1;31m"
  local c_warning="\\033[0;33m"
  local c_highlight="\\033[1;32m"
  local c_notice="\\033[0;01m"
  local c_important="\\033[0;32m"
  local c_subtle="\\033[1;30m"
  local c_prefix="\\"

  local destination="./"
  local failure=0
  local file=
  local name=
  local source="./"
  local verbosity=normal
  local verbose=
  local verbose_common=

  if [[ $# -gt 0 ]] ; then
    t=$#

    while [[ ${i} -lt ${t} ]] ; do
      let i=${i}+1

      if [[ ${SHELL_ENGINE} == "zsh" ]] ; then
        p=${(P)i}
      else
        p=${!i}
      fi

      if [[ ${grab_next} == "" ]] ; then
        if [[ ${p} == "-h" || ${p} == "--help" ]] ; then
          do_help=yes
        elif [[ ${p} == "+C" || ${p} == "++copyright" ]] ; then
          do_copyright="yes"
        elif [[ ${p} == "+d" || ${p} == "++dark" ]] ; then
          do_color=dark
          context="+d"
        elif [[ ${p} == "+l" || ${p} == "++light" ]] ; then
          do_color=light
          context="+l"
        elif [[ ${p} == "+n" || ${p} == "++no_color" ]] ; then
          do_color=none
          context="+n"
        elif [[ ${p} == "+Q" || ${p} == "++quiet" ]] ; then
          verbosity="quiet"
          verbose="+Q"
          verbose_common=
        elif [[ ${p} == "+E" || ${p} == "++error" ]] ; then
          verbosity="error"
          verbose="+E"
          verbose_common=
        elif [[ ${p} == "+N" || ${p} == "++normal" ]] ; then
          verbosity=
          verbose="+N"
          verbose_common=
        elif [[ ${p} == "+V" || ${p} == "++verbose" ]] ; then
          verbosity="verbose"
          verbose="+V"
          verbose_common="-v"
        elif [[ ${p} == "+D" || ${p} == "++debug" ]] ; then
          verbosity="debug"
          verbose="+D"
          verbose_common="-v"
        elif [[ ${p} == "+v" || ${p} == "++version" ]] ; then
          echo ${version}
          return 0
        elif [[ ${p} == "-d" || ${p} == "--destination" ]] ; then
          grab_next=destination
        elif [[ ${p} == "-s" || ${p} == "--source" ]] ; then
          grab_next=source
        else
          name="${p}"
        fi
      else
        if [[ ${grab_next} == "destination" ]] ; then
          destination=$(echo ${p} | sed -e 's|$|/|' -e 's|//*|/|g')
        elif [[ ${grab_next} == "source" ]] ; then
          source=$(echo ${p} | sed -e 's|$|/|' -e 's|//*|/|g')
        fi

        grab_next=
      fi
    done

    p=
  fi

  generate_ctags_handle_colors

  if [[ ${do_help} == "yes" ]] ; then
    generate_ctags_help
    generate_ctags_cleanup

    return 0
  fi

  if [[ ${do_copyright} == "yes" ]] ; then
    generate_ctags_copyright
    generate_ctags_cleanup

    return 0
  fi

  if [[ $(echo "$name" | grep -s -o "/") != "" || ${name} == "" ]] ; then
    if [[ ${verbosity} != "quiet" ]] ; then
      if [[ ${name} == "" ]] ; then
        echo -e "${c_error}ERROR: No ${c_notice}name${c_error} parameter has been provided or is empty.${c_reset}"
      else
        echo -e "${c_error}ERROR: The ${c_notice}name${c_error} parameter value '${c_notice}${name}${c_error}' must not have a slash.${c_reset}"
      fi
    fi

    generate_ctags_cleanup

    return 1
  fi

  if [[ ${destination} == "" || ! -d ${destination} ]] ; then
    if [[ ${verbosity} != "quiet" ]] ; then
      if [[ ${destination} == "" ]] ; then
        echo -e "${c_error}ERROR: The ${c_notice}destination${c_error} parameter is empty.${c_reset}"
      else
        echo -e "${c_error}ERROR: The ${c_notice}destination${c_error} parameter value '${c_notice}${destination}${c_error}' must be a directory.${c_reset}"
      fi
    fi

    generate_ctags_cleanup

    return 1
  fi

  if [[ ${source} == "" || ! -d ${source} ]] ; then
    if [[ ${verbosity} != "quiet" ]] ; then
      if [[ ${source} == "" ]] ; then
        echo -e "${c_error}ERROR: The ${c_notice}source${c_error} parameter is empty.${c_reset}"
      else
        echo -e "${c_error}ERROR: The ${c_notice}source${c_error} parameter value '${c_notice}${source}${c_error}' must be a directory.${c_reset}"
      fi
    fi

    generate_ctags_cleanup

    return 1
  fi

  file="${destination}${name}.tags"

  if [[ ${name} != "" ]] ; then
    if [[ ${verbosity} == "debug" ]] ; then
      echo "find -L ${source} -not -path '*/\.*' \( -name '*.h' \) | ctags -n --fields=EfiklsZSt --extras=-f --c-kinds=+p -L - -f "${file}" --tag-relative=never --language-force=c"
    fi

    find -L ${source} -not -path '*/\.*' \( -name '*.h' \) | ctags -n --fields=EfiklsZSt --extras=-f --c-kinds=+p -L - -f "${file}" --tag-relative=never --language-force=c

    if [[ $? -ne 0 ]] ; then
      if [[ ${verbosity} != "quiet" ]] ; then
        echo -e "${c_error}ERROR: The ${c_notice}ctags${c_error} process failed.${c_reset}"
      fi

      generate_ctags_cleanup

      return 1
    fi

    # Strip out the absolute path.
    if [[ -f "${file}" && ${source} != "./" ]] ; then
      if [[ ${verbosity} == "debug" ]] ; then
        echo "sed -i -e \"s|${source}||g\" \"${file}\""
      fi

      sed -i -e "s|${source}||g" "${file}"

      if [[ $? -ne 0 ]] ; then
        if [[ ${verbosity} != "quiet" ]] ; then
          echo -e "${c_error}ERROR: Failed to strip the full path from the ${c_notice}${file}${c_error} file.${c_reset}"
        fi

        generate_ctags_cleanup

        return 1
      fi
    fi
  fi

  if [[ ${verbosity} != "quiet" ]] ; then
    echo -e "${c_highlight}SUCCESS: The file ${c_notice}${file}${c_highlight} has been generated.${c_reset}"
  fi

  generate_ctags_cleanup

  return 0
}

generate_ctags_handle_colors() {

  if [[ ${do_color} == "light" ]] ; then
    c_error="\\033[1;31m"
    c_warning="\\033[0;31m"
    c_title="\\033[1;34m"
    c_highlight="\\033[0;34m"
    c_notice="\\033[0;01m"
    c_important="\\033[0;35m"
  elif [[ ${do_color} == "none" ]] ; then
    c_reset=
    c_title=
    c_error=
    c_warning=
    c_highlight=
    c_notice=
    c_important=
    c_subtle=
    c_prefix=
  fi
}

generate_ctags_help() {

  echo -e "${c_title}${public_name}${c_reset}"
  echo -e " ${c_notice}Version ${version}${c_reset}"
  echo
  echo -e "${c_highlight}${system_name}${c_reset} ${c_notice}[${c_reset} options ${c_notice}]${c_reset} ${c_notice}[${c_reset} name ${c_notice}]${c_reset}"
  echo
  echo -e "${c_highlight}Options:${c_reset}"
  echo -e " -${c_important}h${c_reset}, --${c_important}help${c_reset}       Print this help message."
  echo -e " +${c_important}C${c_reset}, ++${c_important}copyright${c_reset}  Print the copyright."
  echo -e " +${c_important}d${c_reset}, ++${c_important}dark${c_reset}       Output using colors that show up better on dark backgrounds."
  echo -e " +${c_important}l${c_reset}, ++${c_important}light${c_reset}      Output using colors that show up better on light backgrounds."
  echo -e " +${c_important}n${c_reset}, ++${c_important}no_color${c_reset}   Do not print using color."
  echo -e " +${c_important}Q${c_reset}, ++${c_important}quiet${c_reset}      Decrease verbosity, silencing most print.to."
  echo -e " +${c_important}E${c_reset}, ++${c_important}error${c_reset}      Decrease verbosity, using only error print.to."
  echo -e " +${c_important}N${c_reset}, ++${c_important}normal${c_reset}     Set verbosity to normal."
  echo -e " +${c_important}V${c_reset}, ++${c_important}verbose${c_reset}    Increase verbosity beyond normal print.to."
  echo -e " +${c_important}D${c_reset}, ++${c_important}debug${c_reset}      Enable debugging, significantly increasing verbosity beyond normal print.to."
  echo -e " +${c_important}v${c_reset}, ++${c_important}version${c_reset}    Print only the version number."
  echo
  echo -e "${c_highlight}Generate Options:${c_reset}"
  echo -e " -${c_important}d${c_reset}, --${c_important}destination${c_reset}  The destination directory to write to."
  echo -e " -${c_important}s${c_reset}, --${c_important}source${c_reset}       The source directory containing the header files for generating the ctags from."
  echo
  echo -e "The ${c_notice}name${c_reset} represents the name of the file, without the extension."
}

generate_ctags_copyright() {

  echo "Copyright © 2007-2024 Kevin Day."
  echo
  echo "Source code license lgpl-2.1-or-later."
  echo "Standard and specification license open-standard-license-1.0-or-later."
  echo "Documentation license cc-by-sa-4.0."
}

generate_ctags_cleanup() {

  unset generate_ctags_copyright
  unset generate_ctags_main
  unset generate_ctags_handle_colors
  unset generate_ctags_help
  unset generate_ctags_cleanup
}

main $*
